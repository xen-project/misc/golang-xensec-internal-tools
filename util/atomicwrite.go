// Package util has various utilities
package util

import (
	"fmt"
	"io"
	"os"
	"runtime"
	"sync"
)

type atomicWriteFile struct {
	filename string
	io.Writer
	close func() error
}

func filenameToTemp(filename string) string {
	return filename + ".new"
}

// NewAtomicWriteFile will return a writer which will create a temporary file
// based on the given filename; which when Close()'ed will be renamed to the
// given filename.  This allows "atomic" update to a file from the perspective
// of other readers: any open will either get the old file, or the new file in
// their completion.
func NewAtomicWriteFile(filename string) (io.WriteCloser, error) {
	w, err := os.Create(filenameToTemp(filename))
	if err != nil {
		return nil, err
	}
	awf := atomicWriteFile{
		filename: filename,
		Writer:   w,
		close: sync.OnceValue(func() error {
			return os.Rename(filenameToTemp(filename), filename)
		}),
	}
	runtime.SetFinalizer(&awf, func(awf *atomicWriteFile) { awf.Close() })
	return awf, nil
}

func (awf atomicWriteFile) Close() error {
	return awf.close()
}

func WriteFile(filename string, b []byte) error {
	f, err := NewAtomicWriteFile(filename)
	if err != nil {
		return fmt.Errorf("Opening file %s: %v", filename, err)
	}
	if _, err := f.Write(b); err != nil {
		return err
	}
	return f.Close()
}
