// Package xsagit has functionality related to the Xen Project Security Team's
// `xsa.git` tree; specifically, pulling XSA version and public release
// information from tags in the tree.
package xsagit

import (
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"regexp"
	"slices"
	"strconv"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	parseadvisory "gitlab.com/xen-project/misc/golang-xensec-internal-tools/parse-advisory"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/util"
)

type XsaRepo struct {
	repo *git.Repository
}

type XsaState int

const (
	XsaStateCreated      = XsaState(iota) // No tagged version, TreeAdvisory !Deallocated
	XsaStateDeallocated  = XsaState(iota) // Most recent tagged version is deallocated
	XsaStatePredisclosed = XsaState(iota) // Most recent tagged version is embargoed
	XsaStatePublic       = XsaState(iota) // Most recent tagged version is public
	XsaStateWithdrawn    = XsaState(iota) // Most recent tagged version is withdrawn
)

// VersionInfo contains information about a particular XSA version: The time the
// version was created, the git hash which the version tags, and the contents of
// the advisory at that hash.
type VersionInfo struct {
	When     time.Time
	Hash     plumbing.Hash
	Advisory parseadvisory.Advisory
}

// XSAToSignedAdvisory generates the advisory filename from an XSA number.
func XSAToSignedAdvisory(xsanum int) string {
	return fmt.Sprintf("advisory-%d.txt.asc", xsanum)
}

type XsaInfo struct {
	Xsa int
	// Versions is indexed by "real" version number; i.e., Versions[1] contains
	// information about v1.  This means Versions[0] is empty, and the total
	// number of versions is len(Versions)-1.
	Versions     []VersionInfo
	TreeAdvisory parseadvisory.Advisory // The advisory as checked in at the tip of the tree
	State        XsaState
	// Either the embargo time of the most recent version, or the version time
	// of the first non-embargoed version
	PublicReleaseTime time.Time
}

func (xi *XsaInfo) LatestVersion() *VersionInfo {
	if len(xi.Versions) > 0 {
		return &xi.Versions[len(xi.Versions)-1]
	}
	return nil
}

func Open(path string) (*XsaRepo, error) {
	repo, err := git.PlainOpen(path)
	if err != nil {
		return nil, err
	}

	return &XsaRepo{repo: repo}, nil
}

// var reAdvisoryFileNameSigned = regexp.MustCompile("^advisory-([0-9]+).txt.asc$")
var reAdvisoryFileName = regexp.MustCompile("^advisory-([0-9]+).txt$")
var reTagName = regexp.MustCompile("^xsa-([0-9]+)-v([0-9]+)$")

type getXSARepoOpt struct {
	fileFilters []func(xsanum int, filename string) bool
}

// XSARepoOpt is used to pass options into GetAdvisories
type XSARepoOpt func(*getXSARepoOpt) error

// FilterXSANums will add a filter which only matches the listed XSA numbers.  For example,
// the following will only get XSAs 436 and 437:
//
//	xsas, err := repo.GetAdvisories(xsagit.FilterXSANums(436, 437))
func FilterXSANums(xsanums ...int) XSARepoOpt {
	return func(opt *getXSARepoOpt) error {
		opt.fileFilters = append(opt.fileFilters, func(xsanum int, filename string) bool {
			return !slices.Contains(xsanums, xsanum)
		})
		return nil
	}
}

func (r *XsaRepo) GetFileReaderAtVersion(vi *VersionInfo, filename string) (io.ReadCloser, error) {
	commit, err := r.repo.CommitObject(vi.Hash)
	if err != nil {
		return nil, fmt.Errorf("Getting commit object for hash %v: %w", vi.Hash, err)
	}

	f, err := commit.File(filename)
	if err != nil {
		return nil, fmt.Errorf("Getting file %s at tag %v: %w", filename, vi.Hash, err)
	}

	reader, err := f.Reader()
	if err != nil {
		return nil, fmt.Errorf("Getting reader: %w", err)
	}

	return reader, nil
}

func (r *XsaRepo) WriteVersionFiles(vi *VersionInfo, output string) error {
	commit, err := r.repo.CommitObject(vi.Hash)
	if err != nil {
		return fmt.Errorf("Getting commit object for hash %v: %w", vi.Hash, err)
	}

	for _, fi := range vi.Advisory.Files {
		f, err := commit.File(fi.Name)
		if err != nil {
			return fmt.Errorf("Getting file %s from tag %v: %w", fi.Name, vi.Hash, err)
		}

		reader, err := f.Reader()
		if err != nil {
			return fmt.Errorf("Getting reader: %w", err)
		}
		defer reader.Close()

		relpath := filepath.Dir(fi.Name)
		if err := os.MkdirAll(filepath.Join(output, relpath), fs.ModePerm); err != nil {
			return fmt.Errorf("Creating directory %s: %w", relpath, err)
		}

		ofile, err := util.NewAtomicWriteFile(filepath.Join(output, fi.Name))
		if err != nil {
			return fmt.Errorf("Creating file %s: %w", fi.Name, err)
		}

		if _, err := io.Copy(ofile, reader); err != nil {
			return fmt.Errorf("Copying %s file content: %w", fi.Name, err)
		}

		if err := ofile.Close(); err != nil {
			return err
		}
	}

	return nil
}

// GetAdvisories will parse advisories from the xsa.git repo, collating
// information about versions, current state (Created, Embargoed, Public), and
// public release time (either future of past).
func (r *XsaRepo) GetAdvisories(options ...XSARepoOpt) (map[int]*XsaInfo, error) {
	opt := getXSARepoOpt{}

	for _, option := range options {
		err := option(&opt)
		if err != nil {
			return nil, fmt.Errorf("Parsing options: %v", err)
		}
	}

	// Look through the toplevel of the repo for files matching
	// advisory-NNN.txt; create an entry for each of them
	head, err := r.repo.Head()
	if err != nil {
		return nil, err
	}

	commit, err := r.repo.CommitObject(head.Hash())
	if err != nil {
		return nil, err
	}

	tree, err := commit.Tree()
	if err != nil {
		return nil, err
	}

	xsas := make(map[int]*XsaInfo)
	err = tree.Files().ForEach(func(f *object.File) error {
		if sub := reAdvisoryFileName.FindStringSubmatch(f.Name); len(sub) == 2 {
			xsanum, err := strconv.Atoi(sub[1])
			if err != nil {
				return err
			}
			for _, filter := range opt.fileFilters {
				if filter(xsanum, f.Name) {
					return nil
				}
			}
			xsa := &XsaInfo{Xsa: xsanum}

			if reader, err := f.Reader(); err != nil {
				return err
			} else if a, err := parseadvisory.ParseAdvisory(reader); err == nil {
				xsa.TreeAdvisory = *a
			}

			xsas[xsanum] = xsa
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	// Go through the tags, looking for tags matching xsa-NNN-VV; extract the
	// timestamps and add them to the appropriate versions.
	tags, err := r.repo.Tags()
	if err != nil {
		return nil, err
	}

	if err := tags.ForEach(func(ref *plumbing.Reference) error {
		//fmt.Printf("Processing tag %s\n", ref.Name().Short())
		sub := reTagName.FindStringSubmatch(ref.Name().Short())
		if sub == nil || len(sub) != 3 {
			//fmt.Print(" No match, skipping\n")
			return nil
		}
		xsanum, err := strconv.Atoi(sub[1])
		if err != nil {
			fmt.Printf("Tag %s: Converting xsanum to integer: %v\n", ref.Name().Short(), err)
			return fmt.Errorf("Tag %s: Converting xsanum to integer: %w", ref.Name().Short(), err)
		}
		version, err := strconv.Atoi(sub[2])
		if err != nil {
			fmt.Printf("Tag %s: Converting version to integer: %v\n", ref.Name().Short(), err)
			return fmt.Errorf("Tag %s: Converting version to integer: %w", ref.Name().Short(), err)
		}

		var ts time.Time
		// Tags are of two types: "lightweight" tags which point
		// directly to commits, and tag objects.  The latter may
		// point to a commit, or may point to another tag object;
		// but Log() only takes a commit hash.  Recursively follow
		// tag objects until you get to a commit object.
		//
		// NB we can't use repo.Resolve() here because it won't
		// resolve tags which point to other tags.
		hash := ref.Hash()
	ObjectLoop:
		for {
			obj, err := r.repo.Object(plumbing.AnyObject, hash)
			if err != nil {
				return fmt.Errorf("Error getting object %v (tag %s): %v\n",
					hash, ref.Name(), err)
			}

			switch o := obj.(type) {
			case *object.Commit:
				ts = o.Committer.When
				break ObjectLoop
			case *object.Tag:
				hash = o.Target
			}
		}

		ts = ts.Truncate(time.Minute).UTC()

		if xsa, prs := xsas[xsanum]; !prs {
			if opt.fileFilters == nil {
				return fmt.Errorf("Didn't find xsa %d!", xsanum)
			} else {
				return nil
			}
		} else {
			// Extend Version slice using copy if necessary
			if len(xsa.Versions) < version+1 {
				newVersion := make([]VersionInfo, version+1)
				copy(newVersion, xsa.Versions)
				xsa.Versions = newVersion
			}
			xsa.Versions[version].When = ts
			xsa.Versions[version].Hash = hash
			//fmt.Printf("Tag %s, adding xsa %d version %d time %v\n", ref.Name().Short(), xsanum, version, ts)
		}

		return nil
	}); err != nil {
		return nil, err
	}

	for _, xsa := range xsas {
		if len(xsa.Versions) == 0 {
			xsa.State = XsaStateCreated
		} else {
			// # The public release time is:
			// #  - for an advisory which has not been publicly released,
			// #    the stated embargo time from the most recent pre-disclosed
			// #    version (probably in the future)
			// #  - for an advisory which _has_ been publicly released,
			// #    either
			// #     (i) the version publication time of the earliest
			// #         non-embargoed version; or
			// #     (ii) the stated embargo time of the most recent non-public
			// #         version
			// #    whichever is earlier.
			var MostRecentNonPublicEmbargoTime, EarliestNonEmbargoedVersion time.Time
			for i := range xsa.Versions {
				vi := &xsa.Versions[i]

				if vi.When.IsZero() {
					continue
				}

				reader, err := r.GetFileReaderAtVersion(vi, XSAToSignedAdvisory(xsa.Xsa))
				if err != nil {
					return nil, fmt.Errorf("Getting advisory at v%d: %w", i, err)
				}
				defer reader.Close()

				a, err := parseadvisory.ParseAdvisory(reader)
				if err != nil {
					continue
				}
				vi.Advisory = *a

				if vi.Advisory.Embargoed {
					xsa.State = XsaStatePredisclosed
					MostRecentNonPublicEmbargoTime = a.EmbargoTime
				} else {
					if EarliestNonEmbargoedVersion.IsZero() {
						EarliestNonEmbargoedVersion = vi.When
					}
					if vi.Advisory.Deallocated {
						xsa.State = XsaStateDeallocated
					} else if vi.Advisory.Withdrawn {
						xsa.State = XsaStateWithdrawn
					} else {
						xsa.State = XsaStatePublic
					}
				}
			}

			if !MostRecentNonPublicEmbargoTime.IsZero() &&
				(EarliestNonEmbargoedVersion.IsZero() || MostRecentNonPublicEmbargoTime.Before(EarliestNonEmbargoedVersion)) {
				xsa.PublicReleaseTime = MostRecentNonPublicEmbargoTime
			} else {
				xsa.PublicReleaseTime = EarliestNonEmbargoedVersion
			}
		}
	}

	return xsas, nil
}
