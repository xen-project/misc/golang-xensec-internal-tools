package xsagit_test

import (
	"os"
	"slices"
	"strings"
	"testing"

	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/xsagit"

	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xsalib"
)

func compareAdvisories(t *testing.T, xsa *xsagit.XsaInfo, xsa2 *xsalib.XSAInfo) {
	if len(xsa.Versions) == 0 {
		t.Errorf("XSA-%d: xsa.json has an entry, but we have no tags!  Update xsa.git?", xsa.Xsa)
		return
	}

	la := xsa.Versions[len(xsa.Versions)-1].Advisory

	if xsa2.PublicTime.Time != xsa.PublicReleaseTime {
		t.Errorf("XSA-%d: Unexpected public release time (got %v, wanted %v)",
			xsa.Xsa,
			xsa.PublicReleaseTime,
			xsa2.PublicTime.Time)
		for i := range xsa.Versions {
			if i == 0 {
				continue
			}
			a := &xsa.Versions[i].Advisory
			if a.Xsa == 0 {
				t.Logf(" v%d: No advisory information", i)
				continue
			}
			t.Logf(" v%d: vtime %v embargoed %v embargotime %v",
				i, xsa.Versions[i].When, a.Embargoed, a.EmbargoTime)
		}
	}

	if xsa2.Title == "" {
		// If the public xsa.json has an empty title, it's either embargoed
		// deallocated. The PublicReleaseTime (checked above) should match, but
		// everything else should be empty.
		if xsa2.Deallocated {
			if xsa.State == xsagit.XsaStateDeallocated {
				t.Logf("XSA-%d deallocated", xsa2.XSA)
				return
			}
			t.Errorf("XSA-%d: wanted Deallocated, got %d!", xsa2.XSA, xsa.State)
			return
		}
		t.Logf("XSA-%d predisclosed", xsa2.XSA)
		if xsa.State != xsagit.XsaStatePredisclosed {
			t.Errorf("XSA-%d: xsa.git title empty, expected state %d, got state %d!",
				xsa2.XSA, xsagit.XsaStatePredisclosed, xsa.State)
		}
		return
	}

	// Withdrawn advisories have a title in xsa.json for some reason
	if xsa2.Withdrawn {
		if xsa.State == xsagit.XsaStateWithdrawn {
			t.Logf("XSA-%d withdrawn", xsa2.XSA)
			return
		}
		t.Errorf("XSA-%d: wanted Withdrawn, got %d!", xsa2.XSA, xsa.State)
		return
	}

	// Getting the PublicReleaseTime of an already-public XSA is more
	// tricky; do that in a follow-up patch

	if xsa.State != xsagit.XsaStatePublic {
		t.Errorf("XSA-%d: Expected state %d, got %d!", xsa.Xsa, xsagit.XsaStatePublic, xsa.State)
	}

	if xsa2.Version != len(xsa.Versions)-1 {
		t.Errorf("XSA-%d: Unexpected number of versions (got %d, wanted %d)", xsa.Xsa, len(xsa.Versions)-1, xsa2.Version)
	} else if len(xsa.Versions) > 0 && xsa2.VersionTime.Time != xsa.Versions[len(xsa.Versions)-1].When {
		t.Errorf("XSA-%d: Unexpected version time (got %v, wanted %v)", xsa.Xsa, xsa.Versions[len(xsa.Versions)-1].When, xsa2.VersionTime.Time)
	}

	if xsa2.Title != la.Title {
		t.Errorf("XSA-%d: got Title '%s', wanted '%s'", xsa2.XSA, la.Title, xsa2.Title)
	}

	if len(xsa2.Files) != len(la.Files) {
		t.Errorf("ERROR: XSA-%d: Wanted %d files, got %d", xsa2.XSA, len(xsa2.Files), len(la.Files))
	}

	for i := 0; i < len(xsa2.Files) && i < len(la.Files); i++ {
		if xsa2.Files[i].Name != la.Files[i].Name {
			t.Errorf("ERROR: XSA-%d file %d: Wanted %s got %s",
				xsa2.XSA, i, xsa2.Files[i].Name, la.Files[i].Name)
			continue
		}
	}

	if xsa.TreeAdvisory.Version != xsa2.Version {
		t.Logf("NB: XSA-%d Tree version %v, published version %v", xsa.Xsa, xsa.TreeAdvisory.Version, xsa2.Version)
	} else {
		//t.Logf("XSA-%d: %s", xsa2.XSA, pretty.Sprint(xsa.TreeAdvisory))
		for i := 0; i < len(xsa2.Files) && i < len(xsa.TreeAdvisory.Files); i++ {
			f := xsa.TreeAdvisory.Files[i]
			f2 := xsa2.Files[i]
			if f2.Name != f.Name {
				t.Errorf("ERROR: XSA-%d file %d: Wanted Name %s got %s",
					xsa2.XSA, i, f.Name, f2.Name)
				continue
			}

			if f2.Project != "" {
				if len(f.Projects) > 0 {
					if !strings.EqualFold(f2.Project, string(f.Projects[0])) {
						t.Errorf("ERROR: XSA-%d file %d: Wanted Project %v got %v",
							xsa2.XSA, i, f2.Project, f.Projects)
					}
				} else {
					t.Errorf("ERROR: XSA-%d file %d: Wanted Project %v, got nothing!",
						xsa2.XSA, i, f2.Project)
				}
			} else if len(f.Projects) > 0 {
				t.Logf("NB: XSA-%d file %d, new Projects %v", xsa2.XSA, i, f.Projects)
			}
		}
	}
}

func TestAdvisory(t *testing.T) {
	var xr *xsagit.XsaRepo

	if xsagitPath := os.Getenv("GOTEST_XSAGIT_PATH"); xsagitPath == "" {
		t.Skip("No GOTEST_XSAGIT_PATH set, skipping")
	} else {
		var err error
		xr, err = xsagit.Open(xsagitPath)
		if err != nil {
			t.Fatalf("Opening GOTEST_XSAGIT_PATH %s: %v", xsagitPath, err)
		}
	}

	xsas2, err := xsalib.GetXSAInfo()
	if err != nil {
		t.Logf("Error getting external XSA info (%v), results will not be cross-checked", err)
	}

	t.Log("Testing GetAdvisories(FilterXSANums(432,433))")

	xsas, err := xr.GetAdvisories(xsagit.FilterXSANums(432, 433))
	if err != nil {
		t.Fatalf("Getting advisories: %v", err)
	}

	if len(xsas) != 2 {
		t.Errorf("Expected 2 XSAs, got %d!", len(xsas))
	}

	if xsas2 == nil {
		t.Log("No external XSA info, not cross-checking")
	} else {
		for _, xsa := range xsas {
			i, prs := slices.BinarySearchFunc([]xsalib.XSAInfo(xsas2), xsa, func(xsa2 xsalib.XSAInfo, xsa *xsagit.XsaInfo) int {
				return xsa2.XSA - xsa.Xsa
			})
			if !prs {
				t.Errorf("Missing xsa %d", xsa.Xsa)
				continue
			}
			//t.Logf("Checking XSA-%d", xsa.Xsa)

			compareAdvisories(t, xsa, &xsas2[i])
		}
	}

	t.Log("Testing GetAdvisories()")

	xsas, err = xr.GetAdvisories()
	if err != nil {
		t.Fatalf("Getting advisories: %v", err)
	}

	t.Logf("Got %d advisories", len(xsas))

	for _, xsa2 := range xsas2 {
		xsa, prs := xsas[xsa2.XSA]
		if !prs {
			t.Errorf("Missing xsa %d", xsa2.XSA)
			continue
		}
		//t.Logf("Checking XSA-%d", xsa2.XSA)

		compareAdvisories(t, xsa, &xsa2)

		for i := range xsa.Versions {
			if i == 0 {
				continue
			}
			a := &xsa.Versions[i].Advisory
			if a.Xsa == 0 {
				t.Logf("NB: XSA-%d v%d has empty advisory", xsa.Xsa, i)
			}

			// if len(a.Errors) > 0 {
			// 	t.Logf("NB: XSA-%d v%d has errors %v", xsa.Xsa, i, a.Errors)
			// }

		}

		if len(xsa.TreeAdvisory.Errors) > 0 {
			t.Logf("NB: XSA-%d in-tree has errors %v", xsa.Xsa, xsa.TreeAdvisory.Errors)
		}

	}
}
