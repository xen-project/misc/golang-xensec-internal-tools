package cveapi_test

import (
	"encoding/json"
	"os"
	"path"
	"regexp"
	"slices"
	"strconv"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/cveapi"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/xsagit"
)

func TestCases(t *testing.T) {
	tcdir := "testinput"

	dirents, err := os.ReadDir(tcdir)
	if err != nil {
		t.Errorf("Reading directory %s: %v", tcdir, err)
		return
	}

	reAdvisory := regexp.MustCompile(`^xsa-(\d+)-cve.json?$`)

	tests := map[int]cveapi.CnaContainer{}

	xsanums := []int{}

	// First load all the things
	t.Log("Loading test cases")
	for _, de := range dirents {
		subs := reAdvisory.FindStringSubmatch(de.Name())
		if len(subs) == 0 {
			continue
		}

		filexsanum, err := strconv.Atoi(subs[1])
		if err != nil {
			t.Errorf("Mal-formed advisory file %s: could not parse %s as xsanum", de.Name(), subs[1])
			return
		}

		fpath := path.Join(tcdir, de.Name())
		fbytes, err := os.ReadFile(fpath)
		if err != nil {
			t.Errorf("Opening file %s: %v", fpath, err)
			return
		}

		tcase := cveapi.CnaContainer{}

		if err := json.Unmarshal(fbytes, &tcase); err != nil {
			t.Errorf("Parsing file %s as CnaContainer: %v", fpath, err)
			continue
		}

		tests[filexsanum] = tcase

		xsanums = append(xsanums, filexsanum)
	}

	t.Logf("Loaded %d test cases", len(tests))

	var xr *xsagit.XsaRepo

	if xsagitPath := os.Getenv("GOTEST_XSAGIT_PATH"); xsagitPath == "" {
		t.Skip("No GOTEST_XSAGIT_PATH set, skipping the rest of the test")
	} else {
		var err error
		xr, err = xsagit.Open(xsagitPath)
		if err != nil {
			t.Fatalf("Opening GOTEST_XSAGIT_PATH %s: %v", xsagitPath, err)
		}
	}

	t.Logf("Getting repo info for %v", xsanums)

	xsas, err := xr.GetAdvisories(xsagit.FilterXSANums(xsanums...))
	if err != nil {
		t.Fatalf("Getting repo info for advisories %v: %v", xsanums, err)
		return
	}

	for _, xi := range xsas {
		t.Logf("Checking XSA-%d", xi.Xsa)
		ti := tests[xi.Xsa]
		cnainfo, errs := cveapi.NewCnaContainer(*xi)

		if !cmp.Equal(ti, cnainfo) {
			t.Errorf("ERROR: Mismatch for XSA %d: %v",
				xi.Xsa, cmp.Diff(ti, cnainfo))
		}

		if errs != nil {
			t.Errorf("Errors returned: %v", errs)
		}

	}
}

func TestCasesSubmit(t *testing.T) {
	conn, err := cveapi.NewConnectionFromEnv()
	if err != nil {
		t.Skipf("Couldn't get connection info, skipping: %v", err)
		return

	}

	var xr *xsagit.XsaRepo

	if xsagitPath := os.Getenv("GOTEST_XSAGIT_PATH"); xsagitPath == "" {
		t.Skip("No GOTEST_XSAGIT_PATH set, skipping the rest of the test")
	} else {
		var err error
		xr, err = xsagit.Open(xsagitPath)
		if err != nil {
			t.Fatalf("Opening GOTEST_XSAGIT_PATH %s: %v", xsagitPath, err)
		}
	}

	if conn.URL != cveapi.URLTest {
		t.Errorf("ERROR: conn.URL %s, refusing to run with non-test credentials", conn.URL)
		return
	}

	var filters []xsagit.XSARepoOpt

	// For testing specific XSAs which may be problematic
	//filters = append(filters, xsagit.FilterXSANums(387))

	t.Logf("Getting advisory information from xsa.git")
	xsas, err := xr.GetAdvisories(filters...)
	if err != nil {
		t.Fatalf("Getting repo info for advisories: %v", err)
		return
	}

	// Get all the XSAs and sort them descending
	xsanums := []int{}
	for xsanum := range xsas {
		xsanums = append(xsanums, xsanum)
	}
	slices.SortFunc(xsanums, func(a, b int) int { return b - a })

	total := 0

	for _, xsanum := range xsanums {
		xi := xsas[xsanum]
		// Skip advisories that are not public yet
		if xi.State != xsagit.XsaStatePublic {
			t.Logf("XSA %d in state %v, skipping", xi.Xsa, xi.State)
			continue
		}
		t.Logf("XSA %d in state %v, publishing", xi.Xsa, xi.State)

		// Get CNAInfo from advisory
		cnainfo, errs := cveapi.NewCnaContainer(*xi)
		if errs != nil {
			t.Logf("Errors making container: %v, skipping", errs)
			continue
		}

		// Get a new cvei from the test server
		cveids, err := conn.Reserve(2024, 1, true)
		if err != nil {
			t.Errorf("ERROR Getting cveid: %v", err)
			return
		}

		if len(cveids) != 1 {
			t.Errorf("ERROR Unxepected number of cveids: %d", len(cveids))
			return
		}

		// Submit it as that cveid
		t.Logf("Attempting to submit XSA %d as cve %s to the test server (in real life, would submit as %v)",
			xi.Xsa, cveids[0], xi.Versions[len(xi.Versions)-1].Advisory.Cve)
		resp, err := conn.PutCveCna(cveids[0], cnainfo)

		total++

		t.Logf("Response: %v", resp)
		if err != nil {
			t.Errorf("ERROR Updating CVE content: %v", err)
		} else if resp.Error != nil {
			t.Errorf("ERROR Updating CVEs returned a protocol error: %v %v", *resp.Error, *resp.Message)
			for _, d := range resp.Details.Errors {
				t.Errorf("  Error: %v", d)
			}
		}

		if total > 100 {
			break
		}
	}
}
