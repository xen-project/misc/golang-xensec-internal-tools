// Package cveapi contains functionalty related to submtiting Xen Security
// Advisories to cve.org.
//
// This consists *both* of a partial implementation of the cveapi protocol in
// Go, *and* of code to marshall parsed XSA structures from the parseadvisory
// packages and xsagit into the required cveapi structures.  Arguably this
// should be two separate packages.
package cveapi

import (
	"fmt"
	"slices"
	"strings"
	"time"

	parseadvisory "gitlab.com/xen-project/misc/golang-xensec-internal-tools/parse-advisory"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/xsagit"
)

// Combine a slice of one-string-per-line into a single string with the given
// lines separated by newlines.
func stringsToString(ls []string) string {
	b := strings.Builder{}

	for _, l := range ls {
		fmt.Fprintln(&b, l)
	}

	return b.String()
}

type Version struct {
	Status  string `json:"status"`
	Version string `json:"version"`
}

type Affected struct {
	DefaultStatus string    `json:"defaultStatus"`
	Product       string    `json:"product"`
	Vendor        string    `json:"vendor"`
	Versions      []Version `json:"versions"`
}

type Credit struct {
	Lang  string `json:"lang"`
	Type  string `json:"type"`
	Value string `json:"value"`
}

// CreditFromLines generates a Credit structure from the content of the CREDITS
// section, by setting the language to `en` and the type to `finder`.
func CreditFromLines(ls []string) Credit {
	return Credit{
		Lang:  "en",
		Type:  "finder",
		Value: stringsToString(ls),
	}
}

type Description struct {
	Lang  string `json:"lang"`
	Value string `json:"value"`
}

func DescriptionFromLines(ls []string) Description {
	return Description{
		Lang:  "en",
		Value: stringsToString(ls),
	}
}

// CveDescription returns a suitable description for the entire Cve.  In this an
// optional explanatory note in addition to the ISSUE DESCRIPTION section
func CveDescription(a parseadvisory.Advisory) Description {
	descBuilder := strings.Builder{}
	if len(a.Cve) > 1 {
		descBuilder.Write([]byte(`
[This CNA information record relates to multiple CVEs; the
text explains which aspects/vulnerabilities correspond to which CVE.]

`))
	}

	for _, l := range a.GetSection(parseadvisory.SectionIssueDescription) {
		fmt.Fprintln(&descBuilder, l)
	}

	return Description{
		Lang:  "en",
		Value: descBuilder.String(),
	}
}

type Impact struct {
	Descriptions []Description `json:"descriptions"`
}

type ProviderMetadata struct {
	OrgID string `json:"orgId"`
}

type Reference struct {
	URL string `json:"url"`
}

type CnaContainer struct {
	Title          string        `json:"title"`
	DatePublic     time.Time     `json:"datePublic"`
	Descriptions   []Description `json:"descriptions"`
	Impacts        []Impact      `json:"impacts"`
	Affected       []Affected    `json:"affected,omitempty"`
	Configurations []Description `json:"configurations,omitempty"`
	Workarounds    []Description `json:"workarounds,omitempty"`
	Credits        []Credit      `json:"credits,omitempty"`
	References     []Reference   `json:"references"`
}

// NewCnaContainer takes an XsaInfo and fills out a CnaContainer struct,
// suitable to being submitted to cve.org.  If State is not XsaStatePublic,
// nothing will be filled out and an error will be returned in the array of
// errors.  Otherwise, errors will be propagated from the advisory errors.
func NewCnaContainer(Xsa xsagit.XsaInfo) (CnaContainer, []error) {
	if Xsa.State != xsagit.XsaStatePublic {
		return CnaContainer{}, []error{fmt.Errorf("XSA %d state not public (%d)", Xsa.Xsa, Xsa.State)}
	}

	a := Xsa.Versions[len(Xsa.Versions)-1].Advisory

	errors := a.Errors

	c := CnaContainer{
		Title:        a.Title,
		DatePublic:   Xsa.PublicReleaseTime,
		Descriptions: []Description{CveDescription(a)},
		References:   []Reference{{URL: fmt.Sprintf("https://xenbits.xenproject.org/xsa/advisory-%d.html", Xsa.Xsa)}},
	}

	if credits := a.GetSection(parseadvisory.SectionCredits); len(credits) > 0 {
		c.Credits = []Credit{CreditFromLines(credits)}
	}

	if impacts := a.GetSection(parseadvisory.SectionImpact); len(impacts) > 0 {
		c.Impacts = []Impact{{
			Descriptions: []Description{DescriptionFromLines(impacts)}}}
	}

	if configurations := a.GetSection(parseadvisory.SectionVulnerableSystems); len(configurations) > 0 {
		c.Configurations = []Description{DescriptionFromLines(configurations)}
	}

	if workarounds := a.GetSection(parseadvisory.SectionMitigation); len(workarounds) > 0 {
		c.Workarounds = []Description{DescriptionFromLines(workarounds)}
	}

	products := []parseadvisory.Project{}
	for _, vt := range a.VersionTags {
		for _, product := range vt.Projects {
			if !slices.Contains(products, product) {
				products = append(products, product)
			}
		}
	}

	for _, product := range products {
		c.Affected = append(c.Affected, Affected{
			DefaultStatus: "unknown",
			Product:       string(product),
			Vendor:        product.Upstream(),
			Versions: []Version{{
				Status:  "unknown",
				Version: fmt.Sprintf("consult Xen advisory XSA-%d", Xsa.Xsa),
			}},
		})
	}

	return c, errors
}
