package cveapi

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
)

type URL string

const (
	EnvUser    = "CVE_USER"
	EnvOrg     = "CVE_ORG"
	EnvAPIKey  = "CVE_API_KEY"
	EnvAPIEnv  = "CVE_ENVIRONMENT"
	EnvOrgUUID = "CVE_ORG_UUID"
)

const (
	URLProd = URL(`https://cveawg.mitre.org/api/`)
	URLDev  = URL(`https://cveawg-dev.mitre.org/api/`)
	URLTest = URL(`https://cveawg-test.mitre.org/api/`)
)

const (
	CVEStateReserved  = "RESERVED"
	CVEStatePublished = "PUBLISHED"
	CVEStateRejected  = "REJECTED"
)

const (
	ErrRecordExists       = "CVE_RECORD_EXISTS"
	ErrRecordDoesNotExist = "CVE_RECORD_DNE"
)

type Connection struct {
	Username string
	Org      string
	APIKey   string
	URL      URL
}

// NewConnectionFromEnv attempts to get connection information from the
// environment variables EnvUser, EnvOrg, EnvAPIKey, and EnvAPIEnv.  If any of
// the first three are empty, it will return an error.  If the last one is
// `test` or `prod`, the appropriate URL value will be set; otherwise, an error
// will be returned.
func NewConnectionFromEnv() (*Connection, error) {
	conn := Connection{
		Username: os.Getenv(EnvUser),
		Org:      os.Getenv(EnvOrg),
		APIKey:   os.Getenv(EnvAPIKey),
	}

	if conn.Username == "" {
		return nil, fmt.Errorf("%s undefined", EnvUser)
	}

	if conn.Org == "" {
		return nil, fmt.Errorf("%s undefined", EnvOrg)
	}
	if conn.APIKey == "" {
		return nil, fmt.Errorf("%s undefined", EnvAPIKey)
	}

	switch os.Getenv(EnvAPIEnv) {
	case "test":
		conn.URL = URLTest
	case "prod":
		conn.URL = URLProd
	case "":
		return nil, fmt.Errorf("%s undefined", EnvAPIEnv)
	default:
		return nil, fmt.Errorf("Unsupported %s value %s", EnvAPIEnv, os.Getenv(EnvAPIEnv))
	}

	return &conn, nil
}

// RespWrapper is a structure which contains common elements of many api
// responses; rather than duplicating, this structure can be included without a
// fieldname to collect these elemetns.
type RespWrapper struct {
	Error   *string
	Message *string
	Details []struct {
		Msg      string
		Param    string
		Location string
	}
}

// httpRequest will make a request with the URL and login parameters contained
// within Connection; using the method specified, and at the reqpath request. If
// payload is of type url.Values, the encoded content will be added as query
// parameters to the URL and an empty body will be posted.  Otherwise the
// payload will be marshalled into JSON, and posted as the body.
//
// The response body will be marshalled as JSON into response.
func (conn Connection) httpRequest(method string, reqpath string, payload any, response any) error {
	fullurl := conn.URL
	if fullurl == "" {
		fullurl = URLProd
	}
	fullurl = fullurl + URL(reqpath)

	var pReader io.Reader

	switch t := payload.(type) {
	case url.Values:
		fullurl = fullurl + "?" + URL(t.Encode())
		pReader = nil
	default:
		bPayload, err := json.Marshal(payload)
		if err != nil {
			return fmt.Errorf("Marshalling payload: %w", err)
		}
		pReader = bytes.NewReader(bPayload)
	}

	//fmt.Printf("REQ: Method %s URL %s\n", method, string(fullurl))

	req, err := http.NewRequest(method, string(fullurl), pReader)
	if err != nil {
		return fmt.Errorf("Making request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("CVE-API-KEY", conn.APIKey)
	req.Header.Set("CVE-API-ORG", conn.Org)
	req.Header.Set("CVE-API-USER", conn.Username)

	// Perform the request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("Error sending request: %w", err)
	}
	defer resp.Body.Close()

	// Read the response body
	bResponse, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("Error reading response body: %w", err)
	}

	//fmt.Printf("RES: status %d body %s\n", resp.StatusCode, string(bResponse))

	return json.Unmarshal(bResponse, response)
}

// HealthCheck GETs the `health-check` path; it's a "ping" to ensure that the API
// is up and your credentials are sane
func (conn Connection) HealthCheck() error {
	return conn.httpRequest("GET", "health-check", nil, &struct{}{})
}

type ReserveResp struct {
	CveIds []struct {
		CveID       string `json:"cve_id"`
		CveYear     string `json:"cve_year"`
		OwningCna   string `json:"owning_cna"`
		State       string `json:"state"`
		RequestedBy struct {
			Cna  string
			User string
		} `json:"requested_by"`
		Reserved string
	} `json:"cve_ids"`
	Meta *struct {
		RemainingQuota int `json:"remaining_quota"`
	}
	RespWrapper
}

// ReserveRaw makes a reservation request (POST-ing to `cve-id`) with the listed
// parameters, and returns a strure with the full response.  Only transport
// errors will result in the error value being non-nil; protocol errors will
// result in the Error value being non-nil.
//
// Most callers should probably use Reserve instead.
func (conn Connection) ReserveRaw(year int, amount int, random bool) (ReserveResp, error) {
	params := url.Values{}
	params.Set("cve_year", fmt.Sprintf("%d", year))
	params.Set("amount", fmt.Sprintf("%d", amount))
	params.Set("short_name", conn.Org)
	if random {
		params.Set("batch_type", "non-sequential")
	} else {
		params.Set("batch_type", "sequential")
	}

	var resp ReserveResp

	err := conn.httpRequest("POST",
		"cve-id",
		params,
		&resp)

	return resp, err
}

// Reserve makes a reservation request using ReserveRaw, but then marshals the
// results in a more friendly-to-use manner, collecting the cve-ids into a
// single array, and putting protocol errors into the error structrue.  Callers
// needing further information from the response (such as CveYear, OwningCna,
// and so on) should use ReserveResp instead.
func (conn Connection) Reserve(year int, amount int, random bool) ([]string, error) {
	resp, err := conn.ReserveRaw(year, amount, random)
	if err != nil {
		return nil, err
	}

	if resp.Error != nil {
		b := strings.Builder{}
		b.WriteString("PROTOCOL ERROR:")
		b.WriteString(*resp.Error)
		b.WriteString("\n")
		if resp.Message != nil {
			b.WriteString(*resp.Message)
			b.WriteString("\n")
		}
		if len(resp.Details) > 0 {
			b.WriteString("DETAILS:\n")
			for _, d := range resp.Details {
				b.WriteString("  ")
				b.WriteString(d.Msg)
				b.WriteString(" ")
				b.WriteString(d.Param)
				b.WriteString(" ")
				b.WriteString(d.Location)
				b.WriteString("\n")
			}
		}
		return nil, errors.New(b.String())
	}

	var cveids []string
	for _, cve := range resp.CveIds {
		cveids = append(cveids, cve.CveID)
	}

	return cveids, nil
}

type PutCnaResponse struct {
	Message *string
	Error   *string
	Details struct {
		Errors []struct {
			InstancePath string
			Message      string
		}
	}
}

// PutCveCna will submit the given CnaContianer as content for the given cveid.
// Only transport errors will result in the error value being non-nil; protocol
// errors will result in the Error value being non-nil.
func (conn Connection) PutCveCna(cveid string, cve CnaContainer) (PutCnaResponse, error) {
	var resp PutCnaResponse
	err := conn.httpRequest("POST",
		fmt.Sprintf("cve/%s/cna", cveid),
		map[string]any{
			"cnaContainer": cve,
		},
		&resp,
	)

	return resp, err
}
