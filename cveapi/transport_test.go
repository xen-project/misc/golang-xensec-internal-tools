package cveapi_test

import (
	"testing"

	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/cveapi"
)

func TestHealthCheck(t *testing.T) {
	conn, err := cveapi.NewConnectionFromEnv()
	if err != nil {
		t.Skipf("Couldn't get connection info, skipping: %v", err)
		return
	}

	if conn.URL != cveapi.URLTest {
		t.Errorf("ERROR: conn.URL %s, refusing to run with non-test credentials", conn.URL)
		return
	}

	if err := conn.HealthCheck(); err != nil {
		t.Errorf("ERROR Doing HealthCheck ping: %v", err)
		return
	}
}

func TestReserve(t *testing.T) {
	conn, err := cveapi.NewConnectionFromEnv()
	if err != nil {
		t.Skipf("Couldn't get connection info, skipping: %v", err)
		return
	}

	if conn.URL != cveapi.URLTest {
		t.Errorf("ERROR: conn.URL %s, refusing to run with non-test credentials", conn.URL)
		return
	}

	{
		resp, err := conn.ReserveRaw(2024, 2, false)

		t.Logf("Response: %v", resp)
		if err != nil {
			t.Errorf("ERROR reserving test CVEs: %v", err)
			return
		} else if resp.Error != nil {
			t.Errorf("ERROR Reserving CVEs returned an error: %v %v", *resp.Error, *resp.Message)
			for _, d := range resp.Details {
				t.Errorf("  Details: %v", d)
			}
		} else if len(resp.CveIds) != 2 {
			t.Errorf("ERROR Unexpected number of CVEs: wanted 2, got %d", len(resp.CveIds))
		} else {
			t.Logf("Reserved CVEs: %v", resp.CveIds)
		}
	}

	if _, err := conn.Reserve(2024, -1, true); err == nil {
		t.Errorf("ERROR: Unexpected success getting -1 cves")
	} else {
		t.Logf("Expected error: %v", err)
	}

	if cveids, err := conn.Reserve(2024, 2, true); err != nil {
		t.Errorf("ERROR: Wrapped reservation: %v", err)
	} else if len(cveids) != 2 {
		t.Errorf("ERROR: Wanted 2 cveids, got %d!", len(cveids))
	} else {
		t.Logf("Wrapped reservation: %v", cveids)
	}

}

func TestPutCveCna(t *testing.T) {
	conn, err := cveapi.NewConnectionFromEnv()
	if err != nil {
		t.Skipf("Couldn't get connection info, skipping: %v", err)
		return
	}

	if conn.URL != cveapi.URLTest {
		t.Errorf("ERROR: conn.URL %s, refusing to run with non-test credentials", conn.URL)
		return
	}

	cveids, err := conn.Reserve(2024, 1, true)
	if err != nil {
		t.Errorf("ERROR Getting cveid: %v", err)
		return
	}

	if len(cveids) != 1 {
		t.Errorf("ERROR Unxepected number of cveids: %d", len(cveids))
		return
	}

	{
		resp, err := conn.PutCveCna(cveids[0], cveapi.CnaContainer{})

		t.Logf("Response: %v", resp)
		if err != nil {
			t.Errorf("ERROR Updating CVE content: %v", err)
			return
		} else if resp.Error != nil {
			t.Errorf("ERROR Updating CVEs returned a protocol error: %v %v", *resp.Error, *resp.Message)
			for _, d := range resp.Details.Errors {
				t.Errorf("  Error: %v", d)
			}
		}
	}

}
