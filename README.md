# Xen Project Security Team Internal Tools

This repository contains tools primarily for use internally by the Xen Project
Security Team; they generally require access to our private repository
containing advisory information (which may include unpublished vulnerabilities).

# Command-line tool

The most useful thing you might need if you're looking at this page is the
`xensec` command-line tool.

First, make sure you have `$HOME/go/bin` on your PATH, and Go installed, at
least version `1.18`.

Then install the binary:

```
go install gitlab.com/xen-project/misc/golang-xensec-internal-tools/xensec@latest
```

This command is designed to be run from the `xsa.git` repository; if you want to
run it from a different directory, you can specify the path with the `--xsapath`
argument.

## Checking an advisory

You can check the advisory to make sure the new parsing tool can parse it:

```
xensec advisory check --xsa <xsanum>
```

Examples:

```
$ xensec advisory check --xsa 439
2024/01/05 16:43:43 Opening advisory file advisory-439.txt
2024/01/05 16:43:43 Errors found:
2024/01/05 16:43:43  Couldn't find any version information

$ xensec advisory check --xsa 437
2024/01/05 16:43:50 Opening advisory file advisory-437.txt
2024/01/05 16:43:50 No errors found.
```

## Publishing an advisory

The current version of the tool requires the following enviornment variables to
be set appropriately: `CVE_USER`, `CVE_ORG`, `CVE_API_KEY`, and
`CVE_ENVIRONMENT`.  Contact George for values for these.

Once these are set:

```
xensec cna publish --xsa <xsanum>
```

For example:

```
$ xensec cna publish --xsa 445
2024/01/05 16:34:48 Using XSA path .
2024/01/05 16:34:48 Looking up XSA 445
2024/01/05 16:34:49 Submitting XSA 445 as CVE-2023-46835
2024/01/05 16:34:49   Server message: CVE-2023-46835 record was successfully created. This submission should appear on https://cve.org/ within 15 minutes.
```

If the XSA contains multiple CVEs, it will submit them all:

```
$ xensec cna publish --xsa 444
2024/01/05 16:34:09 Using XSA path .
2024/01/05 16:34:09 Looking up XSA 444
2024/01/05 16:34:10 Submitting XSA 444 as CVE-2023-34327
2024/01/05 16:34:11   Server message: CVE-2023-34327 record was successfully created. This submission should appear on https://cve.org/ within 15 minutes.
2024/01/05 16:34:11 Submitting XSA 444 as CVE-2023-34328
2024/01/05 16:34:11   Server message: CVE-2023-34328 record was successfully created. This submission should appear on https://cve.org/ within 15 minutes.
```

It *should* refuse to submit non-public advisories, but as always be careful.

The tool doesn't currently check that we own the CVEIDs; but the server will
reject attempts to update CVEs that we don't own.

```
$ xensec cna publish --xsa 443
2024/01/05 16:31:08 Using XSA path .
2024/01/05 16:31:08 Looking up XSA 443
2024/01/05 16:31:09 Submitting XSA 443 as CVE-2023-34325
2024/01/05 16:31:09   Server message: CVE-2023-34325 record was successfully created. This submission should appear on https://cve.org/ within 15 minutes.
2024/01/05 16:31:09 Submitting XSA 443 as CVE-2022-4949
2024/01/05 16:31:09 Protocol error: ORG_DOES_NOT_OWN_ID
2024/01/05 16:31:09  Message: XEN does not own CVE-2022-4949
2024/01/05 16:31:09 Submission failed
```