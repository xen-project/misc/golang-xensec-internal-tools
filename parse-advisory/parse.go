// Package parseadvisory contains core functionality for converting the
// advisories -- the "source of truth" for the Xen Project XSAs -- into
// machine-readable information.
//
// The core function of the library is ParseAdvisory.
package parseadvisory

import (
	"bufio"
	"fmt"
	"io"
	"regexp"
	"slices"
	"strconv"
	"strings"
	"time"
	"unicode"

	"github.com/alecthomas/participle/v2"
	"github.com/alecthomas/participle/v2/lexer"
	glob "github.com/ganbarodigital/go_glob"
	"github.com/kr/pretty"
)

// Outstanding anomalies:
//
// XSA-65 has a single combined "MITIGATION AND RESOLUTION" section
//
// XSAs with no files:
//   15 (and thus no PATCH INFORMATION section),
//    41 (non-standard pointers to QEMU hashes),
//   51, 59, 124, 315, 434, 435 (new releases for all supported versions),
//
// XSAs pointing to commit hashes instead (not handled yet):
//   55 (commit list), 60 (commit list), 273, 387, 439 (commit ranges)
//
// XSAs with lists of files for version matching: 155, 179
//
// XSAs with columns: 186, 187
//
// XSAs with glob specifiers which include brace expansions (e.g., foo-{01,02}),
// which aren't actually part of the glob syntax: 93, 98, 118, 138, 243

var ignoreNoFiles = []int{15, 41, 51, 55, 59, 60, 65, 124, 273, 315, 387, 434, 435, 439}

var ignoreNoVersionTags = []int{
	/* XSAs with archaic tags */
	7, 8, 9, 10,
	/* XSAs that actually just have no tags */
	19, 20, 21, 24, 28, 35, 42,
	77, 90, 99, 136, 254, 257, 283, 289,
	/* XSAs that have non-standard verision tags */
	155, 179, /*multiple lists*/
	186, 187, /*columns*/
	272 /*"All versions of Xen"*/}

var ignoreFileNoVersions = []int{
	93, 98, 118, 138, 243, /* Brace expansions */
	98, /* "Additional update..."*/
}

var ignoreMultipleGlobMatch = []int{
	// XSAs where "patch minimization" meant trying to share patches in a
	// "virtual series"
	131, 158, 201, 222, 280, 282,

	// Same patch, different comment
	153,
}

var ignoreUnmatchedGlobs = []int{
	93, 98, 118, 138, 243, /* Brace expansions */
}

type Section struct {
	Name    string
	Content []string
}

type FileInfo struct {
	Name     string
	Versions []Version // Extrapolated from VersionTag.Versions
	Projects []Project // Extrapolated from Versions.  NB we can have multiple project w/in the same upstream.
	Upstream string    // Extrapolated from Versions
}

// type VersionTag struct {
// 	Globs    []string
// 	Versions []Version
// }

type Advisory struct {
	Xsa         int
	Title       string
	Version     int
	Deallocated bool
	Withdrawn   bool
	Embargoed   bool
	EmbargoTime time.Time
	Cve         []string
	// NB Sections is a slice rather than a map so that we can reconstruct the
	// advisory after editing it
	Sections    []Section
	Errors      []error
	Files       []FileInfo
	VersionTags []VersionTag
}

const (
	SectionIssueDescription  = "ISSUE DESCRIPTION"
	SectionImpact            = "IMPACT"
	SectionVulnerableSystems = "VULNERABLE SYSTEMS"
	SectionMitigation        = "MITIGATION"
	SectionResolution        = "RESOLUTION"
	SectionEmbargoDeployment = "DEPLOYMENT DURING EMBARGO"
	SectionCredits           = "CREDITS"
	SectionPatchInformation  = "PATCH INFORMATION"
)

// GetSection will look for the section with the given title, and return the
// content as an array of lines.
func (a Advisory) GetSection(title string) []string {
	for i := range a.Sections {
		if a.Sections[i].Name == title {
			return a.Sections[i].Content
		}
	}
	return nil
}

var reXsaHeader = regexp.MustCompile(`Xen Security Advisory (?:(CVE-[\d-,CVE]*) / )?XSA-([\d]+)$`)
var reXsaVersion = regexp.MustCompile(`^\s+version (\d+)$`)
var reXsaTitle = regexp.MustCompile(`^\s+(\S.*)$`)
var reXsaDealloc = regexp.MustCompile(`^\s+Unused Xen Security Advisory number$`)
var reXsaWithdrawn = regexp.MustCompile(`^\s+Withdrawn Xen Security Advisory number$`)
var reXsaEmbargoed = regexp.MustCompile(`^\s*\*\*\* EMBARGOED UNTIL (.*) \*\*\*`)
var reSectionDelim = regexp.MustCompile("^(=+)$")

// NB only the first is considered canonical; the others are here because some
// embargoed advisories have that format, so we need to parse it to calculate
// PublicReleaseTime.  An error will be noted for all matches past the first.
var dateFormats = []string{
	"2006-01-02 15:04 MST",
	"2006-01-02 15:04:00 MST",
	"2006-01-02 1504 MST",
	"Monday 2006-01-02 15:04:00 MST",
}

func reXsaHeaderMatch(line string) (string, string) {
	sub := reXsaHeader.FindStringSubmatch(line)
	//fmt.Printf("%s: %v\n", line, sub)
	switch len(sub) {
	case 2:
		return "", sub[1]
	case 3:
		return sub[1], sub[2]
	default:
		return "", ""
	}
}

// IsUpper returns false if s contains any letters (`unicode.IsLetter`) which
// are not upper case (`unicode.IsUpper`), and true otherwise.
func IsUpper(s string) bool {
	for _, r := range s {
		if !unicode.IsUpper(r) && unicode.IsLetter(r) {
			return false
		}
	}
	return true
}

// IsBlank returns true if s is empty or contains nothing but space characters,
// as defined by `strings.TrimSpace`
func IsBlank(s string) bool {
	return len(strings.TrimSpace(s)) == 0
}

// ParseAdvisory takes an input and attempts to parse it as an advisory.
// Generally speaking, errors are only returned for circumstances that prevent
// further processing; either IO errors, or egregious format violations which
// probably indicate that the wrong type of data was passed in.
//
// More subtle errors in formating or specification compliance will be returned
// in a.Errors.
func ParseAdvisory(input io.Reader) (a *Advisory, err error) {
	a = &Advisory{
		Version: 1,
	}

	requiredSections := []string{
		SectionImpact,
		SectionVulnerableSystems,
		SectionMitigation,
	}

	scanner := bufio.NewScanner(input)

	nextLine := func() (string, bool) {
		if !scanner.Scan() {
			return "", true
		}
		l := scanner.Text()
		//fmt.Printf("| %s\n", l)
		return l, false
	}

	skipBlank := func(l string) (string, bool) {
		var ret bool
		for IsBlank(l) {
			if l, ret = nextLine(); ret {
				return "", true
			}
		}
		return l, false
	}

	l, ret := nextLine()
	if ret {
		return
	}

	// Skip PGP armoring
	if l == "-----BEGIN PGP SIGNED MESSAGE-----" {
		if l, ret = nextLine(); ret {
			return
		}

		// PGP-signed messages also contain a hash
		if strings.HasPrefix(l, "Hash: ") {
			if l, ret = nextLine(); ret {
				return
			}
		}
	}

	// Skip blank lines
	if l, ret = skipBlank(l); ret {
		return
	}

	if cvestring, xsastring := reXsaHeaderMatch(l); xsastring == "" {
		err = fmt.Errorf("Bad header pattern (%s)", l)
		return
	} else if Xsa, err := strconv.Atoi(xsastring); err != nil {
		return a, fmt.Errorf("Converting XSA string %s to number: %w", xsastring, err)
	} else {
		a.Xsa = Xsa

		if cvestring != "" {
			a.Cve = strings.Split(cvestring, ",")
		}

		if l, ret = nextLine(); ret {
			return a, nil
		}
	}

	// Look for the version number (optional)
	if sub := reXsaVersion.FindStringSubmatch(l); len(sub) == 2 {
		if Version, err := strconv.Atoi(sub[1]); err != nil {
			err = fmt.Errorf("Converting version string %s to number: %w", sub[1], err)
			return a, err
		} else {
			a.Version = Version
		}

		if l, ret = nextLine(); ret {
			return
		}
	} else {
		a.Version = 1
	}

	// Skip blank lines
	if l, ret = skipBlank(l); ret {
		return
	}

	// See if this has been deallocated or withdrawn
	if reXsaDealloc.MatchString(l) {
		a.Deallocated = true
		return
	}

	if reXsaWithdrawn.MatchString(l) {
		a.Withdrawn = true
	}

	// Look for the title
	if sub := reXsaTitle.FindStringSubmatch(l); len(sub) == 2 {
		a.Title = sub[1]

		if l, ret = nextLine(); ret {
			return
		}
	} else {
		return a, fmt.Errorf("Could not find title")
	}

	// Skip blank lines
	if l, ret = skipBlank(l); ret {
		return
	}

	// Look for embargo time (optional)
	if sub := reXsaEmbargoed.FindStringSubmatch(l); len(sub) == 2 {
		a.Embargoed = true

		if sub[1] == "XXXX-XX-XX 12:00 UTC" {
			a.Errors = append(a.Errors, fmt.Errorf("Embargo without time"))
			a.EmbargoTime = time.Now().Add(time.Hour * 24 * 28)
		} else {
			for i, dateFmt := range dateFormats {
				if t, err := time.Parse(dateFmt, sub[1]); err == nil {
					a.EmbargoTime = t
					if i > 0 {
						a.Errors = append(a.Errors, fmt.Errorf("Invalid date format: %v", dateFmt))
					}
				}
			}
			if a.EmbargoTime.IsZero() {
				a.Errors = append(a.Errors, fmt.Errorf("Cannot parse time %s", sub[1]))
			}
		}

		if l, ret = nextLine(); ret {
			return a, nil
		}
	}

	// Break down sections.  Section breaks are defined by lines containing
	// solely of `=`.  The line before this is the section title; the line
	// before the section title and after the underline should be blank.  All
	// lines after that should be put into that section until the next section
	// division.
	//
	// Do this by loading in all lines, looking for section divisions as we go;
	// and then pulling out the entire thing.
	//
	// NB not scanning l here is safe because we need a section title before the
	// delimiter.
	lines := []string{l}
	sectionTitleOffsets := []int{}

	for scanner.Scan() {
		line := scanner.Text()
		// If we've reached the PGP signature at the bottom, stop processing lines
		if line == "-----BEGIN PGP SIGNATURE-----" {
			break
		}

		lines = append(lines, line)
		if reSectionDelim.MatchString(lines[len(lines)-1]) {
			sectionTitleOffsets = append(sectionTitleOffsets, len(lines)-2)
		}
	}

	for i := range sectionTitleOffsets {
		offset := sectionTitleOffsets[i]

		start := offset + 2
		if IsBlank(lines[start]) {
			start++
		} else {
			a.Errors = append(a.Errors, fmt.Errorf("No space after section delimiter"))
		}

		var end int // NB: Not inclusive
		if i+1 < len(sectionTitleOffsets) {
			end = sectionTitleOffsets[i+1]
			if IsBlank(lines[end-1]) {
				end--
			} else {
				a.Errors = append(a.Errors, fmt.Errorf("No space before section delimiter"))
			}
		} else {
			end = len(lines)
		}
		a.Sections = append(a.Sections, Section{Name: lines[offset], Content: lines[start:end]})
		requiredSections = slices.DeleteFunc(requiredSections, func(s string) bool { return s == lines[offset] })
	}

	if a.Version > 1 {
		if len(a.Sections) < 1 || a.Sections[0].Name != fmt.Sprintf("UPDATES IN VERSION %d", a.Version) {
			a.Errors = append(a.Errors, fmt.Errorf("Version %d but no updates for version", a.Version))
		}
	}

	for _, section := range requiredSections {
		a.Errors = append(a.Errors, fmt.Errorf("No %s section", section))
	}

	a.parseFiles(lines)

	if len(a.VersionTags) < 1 {
		a.Errors = append(a.Errors, fmt.Errorf("Couldn't find any version information"))
	}

	return
}

type Project string

// Upstream returns the defined upstream of the project
func (prj Project) Upstream() string {
	switch prj {
	case "Xen", "Linux", "Xapi":
		return string(prj)
	case "qemu-upstream", "qemu-xen", "qemu-xen-traditional":
		return "Qemu"
	case "xenopsd":
		return "Xapi"
	case "linux-2.6.18-xen", "linux-2.6.18-xen.hg":
		return "Linux"
	}

	return ""
}

type ProjectVersion struct {
	Project Project `parser:"@Project"`
	Version string  `parser:"(Space @(VersionString | Master | RCVersionString | AltVersionString))?"`
}

type VersionSingle struct {
	TipVersion     *string         `parser:"@TipVersion"`
	ProjectVersion *ProjectVersion `parser:"| @@"`
	// ProjectVersion *string `parser:"| ProjectVersionString"`
	Version *string `parser:"| @(VersionString | Master | RCVersionString | AltVersionString)"`
	//Project *string `parser:"| @Project"`
}

func (v VersionSingle) Project() Project {
	if v.TipVersion != nil {
		switch *v.TipVersion {
		case "xen-unstable":
			return "Xen"
		}
	}

	if v.ProjectVersion != nil {
		return v.ProjectVersion.Project
	}

	return ""
}

func (v VersionSingle) String() string {
	if v.TipVersion != nil {
		return *v.TipVersion
	}
	if v.ProjectVersion != nil {
		return fmt.Sprintf("%s %s", v.ProjectVersion.Project, v.ProjectVersion.Version)
	}
	if v.Version != nil {
		return *v.Version
	}
	// if v.Project != nil {
	// 	return *v.Project
	// }
	return "<empty>"
}

// NB that we always make a Range, with either one or two elements, rather than
// having either a range or a singleton, because the latter fails to parse; see
// https://github.com/alecthomas/participle/issues/381

type Version struct {
	Range []VersionSingle `parser:"@@ ( VersionDash @@ )?"`
}

func (v Version) String() string {
	switch len(v.Range) {
	case 0:
		return "<empty>"
	case 1:
		return v.Range[0].String()
	case 2:
		return fmt.Sprintf("%v - %v", v.Range[0], v.Range[1])
	default:
		return "<malformed>"
	}
}

type VersionTarget struct {
	Globs   []string `parser:"@Glob ( CommaSpacer @Glob )*"`
	Comment *string  `parser:"| @VersionComment"`
}

type VersionTag struct {
	Target   VersionTarget `parser:"@@"`
	Versions []Version     `parser:"ColSpacer @@ ( CommaSpacer @@ )*"`
	Selector *string       `parser:"((Space | ColSpacer) Selector)?"`
	Comment  *string       `parser:"((Space | ColSpacer) Comment)?"`
	Projects []Project
}

var pVersionTagLine = participle.MustBuild[VersionTag](
	participle.Lexer(lexer.MustSimple([]lexer.SimpleRule{
		{Name: "TipVersion", Pattern: `xen-unstable`},
		{Name: "RCVersionString", Pattern: `\d+\.\d+-RC series`},
		{Name: "VersionString", Pattern: `\d+\.\d+\.x`},
		{Name: "AltVersionString", Pattern: `\d+\.\d+(-rc\d*|\.\d+)?( onwards)?`},
		{Name: "Master", Pattern: `master|unstable`},
		{Name: "Project", Pattern: `Xen|Linux|qemu-upstream|qemu-xen(-traditional)?|Xapi|xenopsd|linux-2\.6\.18-xen(\.hg)?`},
		{Name: "Selector", Pattern: `\[.*\]`},
		{Name: "Glob", Pattern: `xsa\d+[-_/.a-zA-Z0-9?*{,}[\]]+\.patch`},
		{Name: "VersionComment", Pattern: `<.*>`},
		{Name: "VersionDash", Pattern: ` (-|\.\.\.) `},
		{Name: "CommaSpacer", Pattern: ", and | and |, "},
		{Name: "ColSpacer", Pattern: "\\s{2,}|\t"},
		{Name: "Comment", Pattern: `\(.*\)`},
		{Name: "AltGlob", Pattern: `<.*>`},
		{Name: "Space", Pattern: ` `},
		//{"TipVersion", `xen-unstable( - (Xen )?\d+\.\d+(\.x)?)?`},
		//		{"VersionString", `\d+\.\d+\(.x)?( - (Xen )?\d+\.\d+(\.x)?)?`},
	})),
)

var rePatch = regexp.MustCompile(`xsa\d+[-_/.a-zA-Z0-9]*.patch`)

// A file hash section can occur anywhere in the advisory.  It is *usually*
// contained in the RESOLUTION section, but there are some exceptions:
//
// For xsanum <= 18, it's contained in a PATCH INFORMATION section.
//
// XSA-289 has "don't apply these" test patches in a "TECHNICAL DETAILS" section
//
// XSA-254 has information files (some READMEs and a script) in an "ATTACHMENTS"
// section.
//
// The version section, however, must come right before the file hash section.
func (a *Advisory) parseFiles(lines []string) {
	var filehash, version *struct {
		start, end int
	}

	reFileHashSectionStart := regexp.MustCompile(`^\$ sha256sum (.*)$`)

	// Attempt to identify sub-sections, separated by blank lines.  The one
	// matching the regexp above is the filehash section; the one immediately
	// preceding it is the version section.
	//
	// Keep processing after the filehash section is found to make sure there's
	// only one
	i := 0
	for {
		// Skip all blank lines
		for {
			if i >= len(lines) || lines[i] != "" {
				break
			}
			i++
		}

		if i >= len(lines) {
			break
		}

		// Try to identify the section from the first line
		if reFileHashSectionStart.MatchString(lines[i]) {
			if filehash == nil {
				filehash = &struct {
					start int
					end   int
				}{start: i + 1}
			} else {
				a.Errors = append(a.Errors, fmt.Errorf("Unexpected additional filehash section at line %d", i))
			}
		} else if filehash == nil {
			version = &struct {
				start int
				end   int
			}{start: i}
		}

		// Then find the end of the section
		for {
			i++
			if i >= len(lines) || lines[i] == "" {
				break
			}
		}

		if filehash != nil && filehash.end == 0 {
			filehash.end = i - 1
		} else if filehash == nil {
			version.end = i - 1
		}
	}

	if filehash == nil {
		if !slices.Contains(ignoreNoFiles, a.Xsa) {
			a.Errors = append(a.Errors, fmt.Errorf("No files"))
		}
		return
	}

	// After a certain point, file hash sections started being delimeted at
	// the end with another "$"
	if lines[filehash.end] == "$" {
		filehash.end--
	}

	reFileHashLine := regexp.MustCompile(`^([0-9a-f]+)\s+(xsa[0-9]+[-_/.a-zA-Z0-9]*)$`)

	for i := filehash.start; i <= filehash.end; i++ {
		if sub := reFileHashLine.FindStringSubmatch(lines[i]); len(sub) == 0 {
			a.Errors = append(a.Errors, fmt.Errorf("Unexpected file hash: %s", lines[i]))
		} else {
			f := FileInfo{Name: sub[2]}
			a.Files = append(a.Files, f)
		}
	}

	// VERSION SECTIONS
	//
	// XSA-8,9 has the mapping in reverse order (version -> patchname)
	//
	// XSA-155 has some super complicated instructions for a wide range of
	// projcets. it could be converted into a standardized format, but it would
	// be difficult.
	//
	// XSA-186, 187 has a "custom" table with three columns
	//
	// XSA-271, the "version glob" section just has the filename
	//
	// XSA-267 has two "version glob" sections, the second one missing a space
	//
	// XSA-280, 282 has "x.patch + y.patch" instead of a glob
	//
	// XSA-97, 222 have some globs, one "x.patch, y.patch"
	//
	// XSA-381 has `(docs only)` as a comment after `xen-unstable`
	//
	// XSA-370,371: Some lines have "<comment>" in place of a filename
	//
	// XSA-93, 98 use shell pattern expansion ({x,y} or {0..6}), which is not
	// globbing

	for i := 0; i <= version.end; i++ {
		vt, err := pVersionTagLine.ParseString("", lines[i])
		if err != nil {
			// 	if i == version.start {
			// 		if !slices.Contains(knownNoVersionTags, a.Xsa) && !slices.Contains(knownBadVersionTags, a.Xsa) {
			// 			a.Errors = append(a.Errors, fmt.Errorf("Files without version tags: %v", err))
			// 		}
			// 		break
			// 	}
			// 	if !slices.Contains(knownBadVersionTags, a.Xsa) {
			// 		a.Errors = append(a.Errors, fmt.Errorf("Unparseable version at line %d (%v)", i, err))
			// 	}
			continue
		}

		// Filter out invalid globs
		globs := []string{}
		for _, g := range vt.Target.Globs {
			if _, err := glob.NewGlob(g).Match("foo"); err != nil {
				a.Errors = append(a.Errors, fmt.Errorf("Bad glob at line %d (%s)", i, g))
			} else {
				globs = append(globs, g)
			}
		}
		vt.Target.Globs = globs

		// Determine the Upstream for this tag, throwing an error in the case of multiple upstreams
		upstream := ""
		for _, v := range vt.Versions {
			if len(v.Range) > 2 {
				a.Errors = append(a.Errors, fmt.Errorf("INTERNAL: More than two versions in range"))
			}
			vProject := Project("")

			// Make sure a range has only a single project (if anything)
			for _, sv := range v.Range {
				if sv.Project() != "" {
					if vProject == "" {
						vProject = sv.Project()
					} else if vProject /*.Upstream()*/ != sv.Project() /*.Upstream()*/ {
						a.Errors = append(a.Errors,
							fmt.Errorf("Version range has multiple projects: %s",
								pretty.Sprint(v.Range)))
					}
				}
			}

			// Add separate projects, showing an error if different upstreams.
			// We allow multiple Projects from the same Upstream primarily to
			// accomodate patches which apply to both qemu-xen and
			// qemu-upstream; but it could theoretically apply to patches which
			// apply both to Linux and linux-2.16-xen.hg
			if vProject != Project("") {
				if upstream == "" {
					upstream = vProject.Upstream()
				} else if vProject.Upstream() != upstream {
					a.Errors = append(a.Errors,
						fmt.Errorf("Version set has multiple upstreams: %s",
							pretty.Sprint(vt.Versions)))

				}
				if !slices.Contains(vt.Projects, vProject) {
					vt.Projects = append(vt.Projects, vProject)
				}
			}
		}

		if len(vt.Target.Globs) > 0 || vt.Target.Comment != nil {
			a.VersionTags = append(a.VersionTags, *vt)
		}

		if len(vt.Target.Globs) > 0 && vt.Projects == nil {
			a.Errors = append(a.Errors, fmt.Errorf("Version tag has no project: %s", pretty.Sprint(vt.Versions)))
		}
	}

	// Don't bother matching (and noting errors) if there are no versions
	if len(a.VersionTags) == 0 {
		if !slices.Contains(ignoreNoVersionTags, a.Xsa) {
			a.Errors = append(a.Errors, fmt.Errorf("Files without version tags"))
		}
	} else {
		fcount := map[string]struct{}{}
		for i := range a.Files {
			f := &a.Files[i]
			for j := range a.VersionTags {
				vt := &a.VersionTags[j]
				for _, g := range vt.Target.Globs {
					if i == 0 {
						// Instantiate a version so when we check keys later
						fcount[g] = struct{}{}
					}
					if match, _ := glob.NewGlob(g).Match(f.Name); match {
						delete(fcount, g)
						if f.Versions != nil && !slices.Contains(ignoreMultipleGlobMatch, a.Xsa) {
							a.Errors = append(a.Errors, fmt.Errorf("File %s matches additional glob %s", f.Name, g))
						}
						f.Versions = append(f.Versions, vt.Versions...)
						for _, p := range vt.Projects {
							if !slices.Contains(f.Projects, p) {
								f.Projects = append(f.Projects, p)
							}
							if f.Upstream == "" {
								f.Upstream = p.Upstream()
							} else if f.Upstream != p.Upstream() {
								a.Errors = append(a.Errors, fmt.Errorf("File %s matches additional upstream %s", f.Name, p.Upstream()))
							}
						}
					}
				}
			}
			if f.Versions == nil && !slices.Contains(ignoreFileNoVersions, a.Xsa) {
				if rePatch.MatchString(f.Name) {
					a.Errors = append(a.Errors, fmt.Errorf("Patch file %s matches no versions", f.Name))
				}
			}
		}
		if len(fcount) > 0 && !slices.Contains(ignoreUnmatchedGlobs, a.Xsa) {
			unmatched := []string{}
			for k := range fcount {
				unmatched = append(unmatched, k)
			}
			a.Errors = append(a.Errors, fmt.Errorf("Unmatched glob patterns: %v", unmatched))
		}
	}
}
