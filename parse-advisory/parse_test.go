package parseadvisory

import (
	"bytes"
	"errors"
	"os"
	"path"
	"regexp"
	"strconv"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

type ReXsaHeaderResult struct {
	Cves   string
	Xsanum string
}

func TestReXsaHeader(t *testing.T) {
	tests := []struct {
		input string
		want  ReXsaHeaderResult
	}{
		{input: "Xen Security Advisory CVE-2022-26358,CVE-2022-26359,CVE-2022-26360,CVE-2022-26361 / XSA-400",
			want: ReXsaHeaderResult{
				"CVE-2022-26358,CVE-2022-26359,CVE-2022-26360,CVE-2022-26361",
				"400"}},
		{input: "            Xen Security Advisory CVE-2019-18421 / XSA-299",
			want: ReXsaHeaderResult{
				"CVE-2019-18421",
				"299"}},
		{input: "                    Xen Security Advisory XSA-398",
			want: ReXsaHeaderResult{
				"",
				"398"}},
	}

	for _, test := range tests {
		var got ReXsaHeaderResult

		got.Cves, got.Xsanum = reXsaHeaderMatch(test.input)

		if !cmp.Equal(test.want, got) {
			t.Errorf("Failure parsing %s: diff %v\n", test.input, cmp.Diff(test.want, got))
		}
	}
}

func TestParseTopMatter(t *testing.T) {
	tests := []struct {
		input string
		want  Advisory
	}{
		{`
		Xen Security Advisory CVE-2019-18421 / XSA-299
		version 4

  Issues with restartable PV type change operations

ISSUE DESCRIPTION
=================

Description here

IMPACT
======

Impact here
`, Advisory{
			Xsa:     299,
			Title:   "Issues with restartable PV type change operations",
			Version: 4,
			Cve:     []string{"CVE-2019-18421"},
			Sections: []Section{
				{"ISSUE DESCRIPTION", []string{"Description here"}},
				{"IMPACT", []string{"Impact here"}},
			},
			Errors: []error{
				errors.New("Version 4 but no updates for version"),
				errors.New("No VULNERABLE SYSTEMS section"),
				errors.New("No MITIGATION section"),
				errors.New("No files"),
				errors.New("Couldn't find any version information"),
			},
		}},
		{`
		Xen Security Advisory CVE-2019-18421 / XSA-299
		version 4

  Issues with restartable PV type change operations

UPDATES IN VERSION 4
====================

Updates here

ISSUE DESCRIPTION
=================

Description here

IMPACT
======

Impact here
`, Advisory{
			Xsa:     299,
			Title:   "Issues with restartable PV type change operations",
			Version: 4,
			Cve:     []string{"CVE-2019-18421"},
			Sections: []Section{
				{"UPDATES IN VERSION 4", []string{"Updates here"}},
				{"ISSUE DESCRIPTION", []string{"Description here"}},
				{"IMPACT", []string{"Impact here"}},
			},
			Errors: []error{
				errors.New("No VULNERABLE SYSTEMS section"),
				errors.New("No MITIGATION section"),
				errors.New("No files"),
				errors.New("Couldn't find any version information"),
			},
		}},
	}

	for _, test := range tests {
		got, err := ParseAdvisory(bytes.NewBuffer([]byte(test.input)))
		if err != nil {
			t.Errorf("Failure parsing advisory: %v", err)
			continue
		}

		if !cmp.Equal(&test.want, got, cmp.Comparer(func(a, b error) bool { return a.Error() == b.Error() })) {
			t.Errorf("Failure parsing advisory: diff %v", cmp.Diff(&test.want, got, cmpopts.EquateErrors()))
		}
	}
}

func TestParseFiles(t *testing.T) {
	xsadir := os.Getenv("GOTEST_XSA_GIT")
	if xsadir == "" {
		t.Skip("GOTEST_XSA_GIT not set, skipping")
	}

	dirents, err := os.ReadDir(xsadir)
	if err != nil {
		t.Errorf("Reading directory %s: %v", xsadir, err)
		return
	}

	//reAdvisory := regexp.MustCompile(`^advisory-(\d+).txt(.asc)?$`)
	reAdvisory := regexp.MustCompile(`^advisory-(\d+).txt?$`)

	n := 0

	for _, de := range dirents {
		subs := reAdvisory.FindStringSubmatch(de.Name())
		if len(subs) == 0 {
			continue
		}

		filexsanum, err := strconv.Atoi(subs[1])
		if err != nil {
			t.Errorf("Mal-formed advisory file %s: could not parse %s as xsanum", de.Name(), subs[1])
			return
		}

		n++

		fpath := path.Join(xsadir, de.Name())
		f, err := os.Open(fpath)
		if err != nil {
			t.Errorf("Opening file %s: %v", fpath, err)
			return
		}

		a, err := ParseAdvisory(f)

		if err != nil {
			t.Errorf("Parsing advisory %s: %v", fpath, err)
		}

		if a.Xsa != filexsanum {
			t.Errorf("Mismatch: filename xsanum %d, header xsanum %d", filexsanum, a.Xsa)
		}

		if len(a.Errors) > 0 {
			t.Logf("%21s has errors: %v", de.Name(), a.Errors)
		}
	}

	t.Logf("Parsed %d advisories", n)

	if n == 0 {
		t.Errorf("No advisories found!")
	}
}
