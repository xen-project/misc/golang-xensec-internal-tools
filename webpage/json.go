package webpage

import (
	"encoding/json"
	"fmt"
	"io"
	"regexp"
	"slices"
	"strconv"
	"time"

	parseadvisory "gitlab.com/xen-project/misc/golang-xensec-internal-tools/parse-advisory"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/xsagit"
)

// Version ranges can take one of several forms:
//
// ["a.b.x"]: The single major version, all point releases, a.b.0 through a.b.current
// [null, "a.b.x"] All Xen versions through a.b.x
// ["a.b.x", null] All xen versions starting with a.b.x until current version, including master
//
// Naturally, "current" means, "current at the time the XSA was
// issued"; so to find the exact versions you need to know when
// various releases happened.
//
// Also, some strings have "a.b.c", if a vulnerability was introduced
// while backporting something to a stable branch.  For the time being
// this is ignored.

type XSATime struct {
	time.Time
}

const XSATimeFormat = `"2006-01-02 15:04"`

func (xt *XSATime) UnmarshalJSON(b []byte) error {
	var err error
	xt.Time, err = time.Parse(XSATimeFormat, string(b))
	return err
}

func (xt *XSATime) MarshalJSON() ([]byte, error) {
	return []byte(xt.Time.Format(XSATimeFormat)), nil
}

func VersionSingleToVarraySingle(v parseadvisory.VersionSingle) *string {
	if v.TipVersion != nil {
		return v.TipVersion
	}
	if v.ProjectVersion != nil && v.ProjectVersion.Version != "" {
		if v.ProjectVersion.Project == "Xen" && v.ProjectVersion.Version == "unstable" {
			s := "xen-unstable"
			return &s
		}
		return &v.ProjectVersion.Version
	}
	if v.Version != nil {
		return v.Version
	}
	return nil
}

func Push[S ~[]E, E any](s S, e E) S {
	return append([]E{e}, s...)
}

var reVersion = regexp.MustCompile(`^(\d+)\.(\d+)`)

func isMain(s string) bool {
	return s == "xen-unstable" || s == "master" || s == "unstable"
}

func compareVersion(a, b *string) int {
	if a == nil {
		return -1
	}
	if b == nil {
		return 1
	}
	if isMain(*a) {
		return 1
	}
	if isMain(*b) {
		return -1
	}
	var av, bv struct{ major, minor int }
	if sub := reVersion.FindStringSubmatch(*a); len(sub) != 3 {
		return 0
	} else {
		av.major, _ = strconv.Atoi(sub[1])
		av.minor, _ = strconv.Atoi(sub[2])
	}
	if sub := reVersion.FindStringSubmatch(*b); len(sub) != 3 {
		return 0
	} else {
		bv.major, _ = strconv.Atoi(sub[1])
		bv.minor, _ = strconv.Atoi(sub[2])
	}

	if av.major != bv.major {
		return av.major - bv.major
	}
	return av.minor - bv.minor
}

// var reOnwards = regexp.MustCompile(`$(\d+\.\d+(?:-rc\d*|\.\d+)?) onwards`)
var reOnwards = regexp.MustCompile(`(\d+\.\d+(?:-rc\d*|\.\d+)?)(?: onwards)`)
var reRCSeries = regexp.MustCompile(`\d+\.\d+-RC series`)

func VersionToVArray(v parseadvisory.Version) []*string {
	// MADNESS!!!
	var vs []*string
	// Special case "x.y[.z] onwards" as []*string{"x.y.z", nil}
	if len(v.Range) == 1 && v.Range[0].Version != nil {
		if sub := reOnwards.FindStringSubmatch(*v.Range[0].Version); len(sub) == 2 {
			s := sub[1]
			return []*string{&s, nil}
		}
	}
	for _, vv := range v.Range {
		vas := VersionSingleToVarraySingle(vv)
		// Filter out version ranges that contain RC strings
		if vas != nil && reRCSeries.MatchString(*vas) {
			return nil
		}
		vs = Push(vs, vas)
	}
	// Make sure the versions are in order
	if len(vs) == 2 && vs[0] != nil && vs[1] != nil && compareVersion(vs[0], vs[1]) > 0 {
		t := vs[0]
		vs[0] = vs[1]
		vs[1] = t
	}

	return vs
}

type XSAFileInfo struct {
	Name     string      `json:"name"`
	Project  string      `json:"project,omitempty"`
	Upstream string      `json:"upstream,omitempty"`
	Versions [][]*string `json:"versions,omitempty"`
}

// XSAInfo contains information about a given XSA.
//
// XenVersionList and ProjectList are "accumulated" values from Files,
// to make it easier to filter XSAs at the top level.
type XSAInfo struct {
	Deallocated bool          `json:"deallocated,omitempty"`
	Cve         []string      `json:"cve,omitempty"`
	Files       []XSAFileInfo `json:"files,omitempty"`
	PublicTime  XSATime       `json:"public_time"`
	Title       string        `json:"title,omitempty"`
	Version     int           `json:"version,omitempty"`
	VersionTime *XSATime      `json:"version_time,omitempty"`
	Withdrawn   bool          `json:"withdrawn,omitempty"`
	XSA         int           `json:"xsa,string"` // For some reason this is a string rather than an integer
}

// GenerateJSON will take a list of XSAs write out JSON in canonical format to
// the writer.
//
// XSAs and versions are sorted in ascending order.
//
// A few differences to the perl xsa.json code:
//
//   - We expose information for a lot more XSAs
//
//   - cve and file fields were sometimes empty lists `[ ]`, and sometimes
//     ommitted. This turns out not easy to do using golang's json.Marshal(); just
//     omit empty lists all the time.
//
//   - We don't yet expose the "docs_only" flag
func GenerateJSON(xsaOrig []*xsagit.XsaInfo, w io.Writer) error {
	xsas := make([]*xsagit.XsaInfo, len(xsaOrig))

	copy(xsas, xsaOrig)

	slices.SortFunc(xsas, func(a, b *xsagit.XsaInfo) int {
		// Sort in ascending order
		return a.Xsa - b.Xsa
	})

	var XSAS []XSAInfo

	for _, xi := range xsas {
		if xi.State == xsagit.XsaStateCreated {
			// Skip XSAs that nobody has seen
			continue
		}
		jxi := XSAInfo{}
		lv := xi.LatestVersion()

		jxi.XSA = xi.Xsa
		jxi.PublicTime.Time = xi.PublicReleaseTime

		if len(lv.Advisory.Cve) > 0 {
			if lv.Advisory.Embargoed {
				jxi.Cve = []string{"embargoed"}
			} else {
				jxi.Cve = make([]string, len(lv.Advisory.Cve))
				copy(jxi.Cve, lv.Advisory.Cve)
			}
		} else if !(lv.Advisory.Deallocated || lv.Advisory.Withdrawn) {
			jxi.Cve = []string{}
		}

		if !lv.Advisory.Embargoed {
			if lv.Advisory.Deallocated {
				jxi.Deallocated = true
			} else {
				jxi.Version = lv.Advisory.Version
				jxi.VersionTime = &XSATime{Time: lv.When}
				jxi.Title = lv.Advisory.Title
				jxi.Withdrawn = lv.Advisory.Withdrawn
			}

			for _, fi := range lv.Advisory.Files {
				jxif := XSAFileInfo{
					Name: fi.Name,
				}
				if len(fi.Projects) == 1 {
					jxif.Project = string(fi.Projects[0])
					jxif.Upstream = fi.Projects[0].Upstream()
					if jxif.Project == "Xen" {
						// For some reason, only Xen is has a lower-case Project name
						jxif.Project = "xen"
					}
				}
				for _, v := range fi.Versions {
					if varray := VersionToVArray(v); varray != nil {
						jxif.Versions = append(jxif.Versions, varray)
					}
				}
				if len(jxif.Versions) > 1 {
					slices.SortFunc(jxif.Versions, func(a, b []*string) int {
						return compareVersion(a[0], b[0])
					})
				}
				jxi.Files = append(jxi.Files, jxif)
			}
		}

		XSAS = append(XSAS, jxi)
	}

	// For some reason, there's an extra two layers here...
	XSAJSONStruct := []struct {
		XSAS []XSAInfo `json:"xsas"`
	}{
		{XSAS: XSAS},
	}

	b, err := json.Marshal(XSAJSONStruct)
	if err != nil {
		return fmt.Errorf("Marshalling json: %v", err)
	}

	if _, err := w.Write(b); err != nil {
		return fmt.Errorf("Writing out json: %w", err)
	}

	return nil
}
