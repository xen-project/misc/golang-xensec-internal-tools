<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head>
<title>XSA-{{.XSA}} - Xen Security Advisories</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<h1>Information</h1>
<table>
<tr><th>Advisory</th><td><a href="advisory-{{.XSA}}.html">XSA-{{.XSA}}</a></td></tr>
<tr><th>Public release</th> <td>{{.PublicRelease}}</td></tr>
<tr><th>Updated</th> <td>{{.Updated}}</td></tr>
<tr><th>Version</th> <td>{{.Version}}</td></tr>
<tr><th>CVE(s)</th> <td>{{if .Cves}}{{range .Cves}}<a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name={{.}}">{{.}}</a> {{end -}}
{{else}}{{.CveAlt}}{{end}}</td></tr>
<tr><th>Title</th> <td>{{.Title}}</td></tr>
</table>

<h1>Files</h1>
<a href="advisory-{{.XSA}}.txt">advisory-{{.XSA}}.txt</a> (signed advisory file)<br />
{{range .Files}}
<a href="{{.}}">{{.}}</a><br />
{{end}}

<h1>Advisory</h1>
<hr />
<pre>
{{.AdvisoryTextWithLinks}}
</pre>
<hr /><address>Xenproject.org Security Team</address>
</body>
</html>
