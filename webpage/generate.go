// Package webpage contains functions related to generating the XSA information webpage
// at https://xenbits.xenproject.org/xsa
package webpage

import (
	_ "embed"
	"fmt"
	"html/template"
	"io"
	"path"
	"regexp"
	"slices"
	"strings"

	parseadvisory "gitlab.com/xen-project/misc/golang-xensec-internal-tools/parse-advisory"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/util"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/xsagit"
	"golang.org/x/net/html"
)

// TODO:
// [x] Use signed advisory for versions when available
// [x] Write advisory-NNN.txt (with signed advisory text)
// [x] Copy files
// [x] Add links to files under advisory-NNN.html Files section
// [x] Scan withdrawn advisories for files
// [x] Look for links in the advisory text and linkify them
// [x] Linkify files in the advisory text
// [-] Generate xsa.json
//   [ ] docs_only (XSA-370)
//   [x] "onwards" (XSA-370)
// [x] Do write-copy to avoid transient 404s

//go:embed xsaIndex.gtpl
var xsaIndexGtpl string
var xsaIndexTmpl = template.Must(template.New("XsaIndex").Parse(xsaIndexGtpl))

//go:embed advisory.gtpl
var advisoryGtpl string
var advisoryTmpl = template.Must(template.New("Advisory").Parse(advisoryGtpl))

type webinfo struct {
	XSA           int
	AdvisoryLink  bool
	Title         string
	PublicRelease string
	Updated       string
	Version       string
	Cves          []string
	CveAlt        string

	// Used by advisory-NNN.html
	AdvisoryTextWithLinks template.HTML
	Files                 []string
}

const (
	dateFormat    = "2006-01-02 15:04"
	indexFilename = "index.html"
	jsonFilename  = "xsa.json"
)

func GeneratePublicWebpage(xsapath, output string) error {
	xr, err := xsagit.Open(xsapath)
	if err != nil {
		return fmt.Errorf("opening xsa.git path %s: %w", xsapath, err)
	}

	xsamap, err := xr.GetAdvisories()
	if err != nil {
		return fmt.Errorf("getting advisories: %w", err)
	}

	xsaslice := make([]*xsagit.XsaInfo, 0, len(xsamap))

	for _, xi := range xsamap {
		if xi.Xsa >= 26 {
			xsaslice = append(xsaslice, xi)
		}
	}

	slices.SortFunc(xsaslice, func(a, b *xsagit.XsaInfo) int {
		// Sort in reverse order
		return b.Xsa - a.Xsa
	})

	wis := make([]*webinfo, 0, len(xsamap))

	pretendEmbargoed := 0

	for _, xi := range xsaslice {
		var wi *webinfo
		lv := xi.LatestVersion()

		if pretendEmbargoed > 0 {
			xi.State = xsagit.XsaStatePredisclosed
			pretendEmbargoed--
		}
		switch xi.State {
		case xsagit.XsaStateCreated:
			// Don't do anything with pages with no versions
			continue
		case xsagit.XsaStatePredisclosed:
			// Generate "embargoed" webpage
			wi = &webinfo{
				XSA:           xi.Xsa,
				Title:         "(Preleased, but embargoed)",
				PublicRelease: xi.PublicReleaseTime.Format(dateFormat),
			}
			if len(lv.Advisory.Cve) > 0 {
				wi.CveAlt = "assigned, but embargoed"
			} else {
				wi.CveAlt = "none (yet) assigned"
			}

			// Generate "embargoed" json info
		case xsagit.XsaStateDeallocated:
			// Generate webpage
			wi = &webinfo{
				XSA:           xi.Xsa,
				AdvisoryLink:  true,
				Title:         "Unused Xen Security Advisory number",
				Version:       "-",
				PublicRelease: xi.PublicReleaseTime.Format(dateFormat),
				CveAlt:        "-",
			}
			// Generate json info
			// Generate advisory-NNN.html
			// Copy advisory-NNN.txt.asc
		case xsagit.XsaStateWithdrawn:
			wi = &webinfo{
				XSA:           xi.Xsa,
				AdvisoryLink:  true,
				Title:         "Withdrawn Xen Security Advisory number",
				Version:       fmt.Sprintf("%d", lv.Advisory.Version),
				PublicRelease: xi.PublicReleaseTime.Format(dateFormat),
				Updated:       lv.When.Format(dateFormat),
				CveAlt:        "-",
			}
			// Generate json info
			// Generate advisory-NNN.html
			// Copy advisory-NNN.txt.asc
		case xsagit.XsaStatePublic:
			// Generate webpage
			wi = &webinfo{
				XSA:           xi.Xsa,
				AdvisoryLink:  true,
				Title:         lv.Advisory.Title,
				Version:       fmt.Sprintf("%d", lv.Advisory.Version),
				PublicRelease: xi.PublicReleaseTime.Format(dateFormat),
				Updated:       lv.When.Format(dateFormat),
			}
			if len(lv.Advisory.Cve) > 0 {
				wi.Cves = lv.Advisory.Cve
			} else {
				wi.CveAlt = "none (yet) assigned"
			}
			// Generate json info
			// Generate advisory-NNN.html
			// Copy advisory-NNN.txt.asc, files
		}

		wis = append(wis, wi)

		if xi.State != xsagit.XsaStatePredisclosed {
			if err := xr.WriteVersionFiles(lv, output); err != nil {
				return fmt.Errorf("Writing files for xsa %d: %w", xi.Xsa, err)
			}
			if err := writeAdvisoryHTML(xr, output, lv, wi); err != nil {
				return fmt.Errorf("Writing advisory-NNN.html: %w", err)
			}
		}
	}

	wfile, err := util.NewAtomicWriteFile(path.Join(output, indexFilename))
	if err != nil {
		return fmt.Errorf("creating index file %s: %w", indexFilename, err)
	}
	if err := xsaIndexTmpl.Execute(wfile, wis); err != nil {
		return fmt.Errorf("executing template: %w", err)
	}
	if err := wfile.Close(); err != nil {
		return fmt.Errorf("Renaming fiile %s: %w", indexFilename, err)
	}

	jfile, err := util.NewAtomicWriteFile(path.Join(output, jsonFilename))
	if err != nil {
		return fmt.Errorf("creating json file %s: %w", jsonFilename, err)
	}
	if err := GenerateJSON(xsaslice, jfile); err != nil {
		return fmt.Errorf("Marshalling JSON: %w", err)
	}
	if err := jfile.Close(); err != nil {
		return fmt.Errorf("Renaming fiile %s: %w", jsonFilename, err)
	}

	return nil
}

var urlRegex = regexp.MustCompile(`https?://\S+\b`)

func writeAdvisoryHTML(xr *xsagit.XsaRepo, output string, lv *xsagit.VersionInfo, wi *webinfo) error {
	xsa := lv.Advisory.Xsa
	{
		// Fish out the original, signed advisory from xsa.git
		reader, err := xr.GetFileReaderAtVersion(lv, xsagit.XSAToSignedAdvisory(xsa))
		if err != nil {
			return fmt.Errorf("Getting advisory text: %w", err)
		}
		b, err := io.ReadAll(reader)
		if err != nil {
			return fmt.Errorf("Reading advisory text: %w", err)
		}
		reader.Close()

		// Write it out, but with the `.asc` dropped from the end
		advisoryTxtFilename := fmt.Sprintf("advisory-%d.txt", xsa)
		if err := util.WriteFile(path.Join(output, advisoryTxtFilename), b); err != nil {
			return fmt.Errorf("Writing raw advisory text file %s to directory: %w", advisoryTxtFilename, err)
		}

		// Do HTML escaping so that we can add in our own HTML afterwards
		escapedText := html.EscapeString(string(b))

		// Replace all URLs with links
		escapedText = urlRegex.ReplaceAllStringFunc(escapedText, func(url string) string {
			return fmt.Sprintf(`<a href="%s">%s</a>`, url, url)
		})

		// Replace SHA256-file lines with links
		hashAndFileRegex := regexp.MustCompile(`(?m)^[0-9a-f]+\s+(\S+)$`)
		escapedText = hashAndFileRegex.ReplaceAllStringFunc(escapedText, func(match string) string {
			parts := strings.Fields(match)
			if len(parts) != 2 {
				return match // If it does not match the expected pattern exactly, return the original match
			}
			filename := parts[1]
			if slices.ContainsFunc(lv.Advisory.Files, func(fi parseadvisory.FileInfo) bool {
				return fi.Name == filename
			}) {
				return fmt.Sprintf(`<a href="%s">%s</a>`, filename, match)
			}
			return match // Return the original match if no file is found
		})

		wi.AdvisoryTextWithLinks = template.HTML(escapedText)
	}

	// Preallocate space
	wi.Files = make([]string, len(lv.Advisory.Files))
	for i, fi := range lv.Advisory.Files {
		wi.Files[i] = fi.Name
	}

	advisoryHTMLFilename := fmt.Sprintf("advisory-%d.html", xsa)
	afile, err := util.NewAtomicWriteFile(path.Join(output, advisoryHTMLFilename))
	if err != nil {
		return fmt.Errorf("creating index file %s: %w", advisoryHTMLFilename, err)
	}
	if err := advisoryTmpl.Execute(afile, wi); err != nil {
		return fmt.Errorf("executing template: %w", err)
	}
	if err := afile.Close(); err != nil {
		return err
	}

	return nil
}
