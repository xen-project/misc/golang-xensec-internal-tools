package webpage_test

import (
	"io"
	"os"
	"testing"

	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/webpage"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/xsagit"
	"gitlab.com/xen-project/people/gdunlap/go-xenlib/xsalib"
)

func TestJson(t *testing.T) {
	xsapath := os.Getenv("GOTEST_XSAGIT_PATH")
	if xsapath == "" {
		t.Skip("No GOTEST_XSAGIT_PATH set, skipping")
	}

	xr, err := xsagit.Open(xsapath)
	if err != nil {
		t.Errorf("ERROR: opening xsa.git path %s: %v", xsapath, err)
		return
	}

	xsamap, err := xr.GetAdvisories()
	if err != nil {
		t.Errorf("ERROR: getting advisories: %v", err)
		return
	}

	xsaslice := make([]*xsagit.XsaInfo, 0, len(xsamap))

	for _, xi := range xsamap {
		if xi.Xsa >= 26 && xi.State > xsagit.XsaStateCreated {
			xsaslice = append(xsaslice, xi)
		}
	}

	ojson, err := os.CreateTemp("", "*xsa.json")
	if err != nil {
		t.Errorf("ERROR: Creating temporary file: %v", err)
		return
	}

	if err := webpage.GenerateJSON(xsaslice, ojson); err != nil {
		t.Errorf("ERROR: GenerateJSON returned %v", err)
		return
	}

	t.Logf("Wrote json to %s", ojson.Name())

	t.Logf("Re-reading and re-parsing")
	if _, err := ojson.Seek(0, 0); err != nil {
		t.Errorf("ERROR Seeking to beginning of file: %v", err)
	} else if b, err := io.ReadAll(ojson); err != nil {
		t.Errorf("ERROR: Re-reading json: %v", err)
	} else if xsas, err := xsalib.GetXSAInfo(xsalib.Bytes(b)); err != nil {
		t.Errorf("ERROR: Re-parsing json: %v", err)
	} else {
		if len(xsas) != len(xsaslice) {
			t.Errorf("ERROR: after reparsing: Expecting %d XSAs, got %d!",
				len(xsaslice), len(xsas))
		}
	}
}
