<!DOCTYPE html
      PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head>
<title>Xen Security Advisories</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<h1>Advisories, publicly released or pre-released</h1>All times are in UTC.
For general information about Xen and security see
the <a href="http://www.xenproject.org">Xen Project website</a> and
<a href="http://www.xenproject.org/security-policy.html">security policy</a>.
A
<a href="xsa.json">JSON document</a>
listing advisories is also available.
<p>
<table rules="all"><tr><th>Advisory</th> <th>Public release</th> <th>Updated</th> <th>Version</th> <th>CVE(s)</th> <th>Title</th></tr>
{{range . -}}
<tr><td>{{if .AdvisoryLink}}<a href="advisory-{{.XSA}}.html">{{end}}XSA-{{.XSA}}{{if .AdvisoryLink}}</a>{{end}}</td>
 <td>{{.PublicRelease}}</td> <td>{{.Updated}}</td>
<td>{{.Version}}</td>
<td>{{if .Cves}}{{range .Cves}}<a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name={{.}}">{{.}}</a> {{end -}}
{{else}}{{.CveAlt}}{{end}}</td>
<td>{{.Title}}</td></tr>
{{- end}}
</table>Older advisories are not listed here.
<hr /><address>Xenproject.org Security Team</address>
</body>
</html>