package webpage_test

import (
	"os"
	"testing"

	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/webpage"
)

func TestPublicWebpage(t *testing.T) {
	xsapath := os.Getenv("GOTEST_XSAGIT_PATH")
	if xsapath == "" {
		t.Skip("No GOTEST_XSAGIT_PATH set, skipping")
	}

	output, err := os.MkdirTemp("", "generatePublicWebpage")
	if err != nil {
		t.Errorf("Creating temporary output directory: %v", err)
	}

	t.Logf("Generating webpage in directory %s", output)

	if err := webpage.GeneratePublicWebpage(xsapath, output); err != nil {
		t.Errorf("ERROR generating webpage: %v", err)
	}
}
