/*
Copyright © 2024 Cloud Software Group

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package cmd

import (
	"log"
	"os"
	"path"

	"github.com/spf13/cobra"
	parseadvisory "gitlab.com/xen-project/misc/golang-xensec-internal-tools/parse-advisory"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/xsagit"
)

// advisoryCheckCmd represents the check command
var advisoryCheckCmd = &cobra.Command{
	Use:   "check",
	Short: "Parse an advisory and check for potential errors",
	Long: `Parse will read the current file from xsapath/advisory-NNN.txt and attempt
to parse it as an advisory.  If it finds any errors, it will print them out and
return non-zero; otherwise it will return zero.
`,
	Run: func(cmd *cobra.Command, args []string) {
		xsanum, err := cmd.Flags().GetInt(flagXSA)
		if err != nil {
			log.Fatalf("Unable to parse XSA: %v", err)
		}
		if xsanum < 1 {
			log.Fatalf("You must supply an --xsa argument")
		}

		fname := path.Join(xsapath, xsagit.XSAToSignedAdvisory(xsanum))

		log.Printf("Opening advisory file %s", fname)

		if file, err := os.Open(fname); err != nil {
			log.Fatalf("Opening advisory file %s: %v", fname, err)
		} else if a, err := parseadvisory.ParseAdvisory(file); err != nil {
			log.Fatalf("Parsing advisory file %s: %v", fname, err)
		} else if len(a.Errors) > 0 {
			log.Print("Errors found:")
			for _, e := range a.Errors {
				log.Printf(" %v", e)
			}
			os.Exit(1)
		} else {
			log.Printf("No errors found.")
		}
	},
}

func init() {
	advisoryCmd.AddCommand(advisoryCheckCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// checkCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// checkCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
