/*
Copyright © 2024 Cloud Software Group

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/webpage"
)

// generatePublicWebpageCmd represents the generatePublicWebpage command
var generatePublicWebpageCmd = &cobra.Command{
	Use:   "generate-public-webpage",
	Short: "Generate the public webpage for advisories",
	Long: `Output structure:

  index.html :          Lists advisories, numbers, versions, etc.  Links to advisory-NN.html
  xsa.json :            Public JSON index document
  advisory-NN.html :    Text in <pre>.  Links to NN.txt.asc files
  advisory-NN.txt.asc : Original signed advisory
  <files> :             Files included in advisories
`,
	Run: func(cmd *cobra.Command, args []string) {
		output, err := cmd.Flags().GetString("output")
		if err != nil {
			log.Fatalf("Getting output directory argument: %v", err)
		}

		if fi, err := os.Stat(output); err != nil {
			log.Fatalf("Stat'ing output directory %s: %v", output, err)
		} else if !fi.IsDir() {
			log.Fatalf("%s is not a directory", output)
		}

		if err := webpage.GeneratePublicWebpage(xsapath, output); err != nil {
			log.Fatalf("Generating webpage: %v", err)
		}

	},
}

func init() {
	rootCmd.AddCommand(generatePublicWebpageCmd)

	generatePublicWebpageCmd.Flags().StringP("output", "o", "", "Directory into which to output the webpage contents")
	generatePublicWebpageCmd.MarkFlagRequired("output")
}
