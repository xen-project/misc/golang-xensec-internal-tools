/*
Copyright © 2023 Cloud Software Group

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/cveapi"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/xsagit"
)

// cnaGenerateJSONCmd represents the generateJson command
var cnaGenerateJSONCmd = &cobra.Command{
	Use:   "generate-json --xsa <xsanum>",
	Short: "Generate CNA JSONv5 for the given XSA, suitable for submitting to cve.org",
	Run:   cnaGenerateJSON,
}

func cnaGenerateJSON(cmd *cobra.Command, args []string) {
	xsanum, err := cmd.Flags().GetInt(flagXSA)
	if err != nil {
		log.Fatalf("Unable to parse XSA: %v", err)
	}
	if xsanum < 1 {
		log.Fatalf("You must supply an --xsa argument")
	}

	log.Printf("Using XSA path %s", xsapath)
	xr, err := xsagit.Open(xsapath)
	if err != nil {
		log.Fatalf("Opening xsa.git: %v", err)
	}

	log.Printf("Looking up XSA %d", xsanum)
	xsas, err := xr.GetAdvisories(xsagit.FilterXSANums(xsanum))
	if err != nil {
		log.Fatalf("Getting advisory: %v", err)
	}

	switch len(xsas) {
	case 0:
		log.Fatalf("Couldn't find advisory %d", xsanum)
	case 1:
		break
	default:
		log.Fatalf("INTERNAL ERROR: got %d xsas!", len(xsas))
	}

	cnainfo, errs := cveapi.NewCnaContainer(*xsas[xsanum])

	for _, err := range errs {
		log.Printf("Error: %v", err)
	}

	// Convert cnainfo to JSON with indentation
	jsonData, err := json.MarshalIndent(cnainfo, "", "    ")
	if err != nil {
		log.Fatalf("Error marshalling JSON: %v", err)
	}

	// Output the JSON to stdout
	fmt.Println(string(jsonData))

	if errs != nil {
		os.Exit(1)
	}
}
func init() {
	cnaCmd.AddCommand(cnaGenerateJSONCmd)
	cnaGenerateJSONCmd.PersistentFlags().Int(flagXSA, -1, "XSA number to act upon")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateJsonCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateJsonCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
