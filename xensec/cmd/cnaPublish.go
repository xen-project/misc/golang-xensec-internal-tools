/*
Copyright © 2024 Cloud Software Group

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Package cmd implements commands for the xensec binary
package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/cveapi"
	"gitlab.com/xen-project/misc/golang-xensec-internal-tools/xsagit"
)

const (
	flagForce = "force"
)

// cnaPublishCmd represents the publish command
var cnaPublishCmd = &cobra.Command{
	Use:   "publish",
	Short: "Publish the most recent public version of a CVE",
	Run: func(cmd *cobra.Command, args []string) {
		xsanum, err := cmd.Flags().GetInt(flagXSA)
		if err != nil {
			log.Fatalf("Unable to parse flag %s: %v", flagXSA, err)
		}
		if xsanum < 1 {
			log.Fatalf("You must supply an --xsa argument")
		}

		force, err := cmd.Flags().GetBool(flagForce)
		if err != nil {
			log.Fatalf("Unable to parse flag %s: %v", flagForce, err)
		}

		conn, err := cveapi.NewConnectionFromEnv()
		if err != nil {
			log.Fatalf("Getting cveapi connection information: %v", err)
		}

		log.Printf("Using XSA path %s", xsapath)
		xr, err := xsagit.Open(xsapath)
		if err != nil {
			log.Fatalf("Opening xsa.git: %v", err)
		}

		log.Printf("Looking up XSA %d", xsanum)
		xsas, err := xr.GetAdvisories(xsagit.FilterXSANums(xsanum))
		if err != nil {
			log.Fatalf("Getting advisory: %v", err)
		}

		switch len(xsas) {
		case 0:
			log.Fatalf("Couldn't find advisory %d", xsanum)
		case 1:
			break
		default:
			log.Fatalf("INTERNAL ERROR: got %d xsas!", len(xsas))
		}

		xi := xsas[xsanum]

		if xi.State != xsagit.XsaStatePublic {
			log.Fatalf("XSA %d not public (state %d), refusing to publish", xi.Xsa, xi.State)
		}

		cnainfo, errs := cveapi.NewCnaContainer(*xsas[xsanum])

		for _, err := range errs {
			log.Printf("Error: %v", err)
		}

		if len(errs) > 0 {
			if force {
				log.Printf("Force command supplied, ignoring errors")
			} else {
				log.Fatalf("Not continuing due to errors.  Use --force to override.")
			}
		}

		lv := xi.Versions[len(xi.Versions)-1]

		for _, cveid := range lv.Advisory.Cve {
			log.Printf("Submitting XSA %d as %s", xi.Xsa, cveid)
			resp, err := conn.PutCveCna(cveid, cnainfo)
			if err != nil {
				log.Fatalf("Error submitting: %v", err)
			}

			if resp.Error != nil {
				log.Printf("Protocol error: %s", *resp.Error)
				if resp.Message != nil {
					log.Printf(" Message: %s", *resp.Message)
				}
				for _, d := range resp.Details.Errors {
					log.Printf(" Detail: %v", d)
				}
				log.Fatalf("Submission failed")
			}

			log.Printf("  Server message: %s", *resp.Message)
		}
	},
}

func init() {
	cnaCmd.AddCommand(cnaPublishCmd)
	cnaPublishCmd.PersistentFlags().Int(flagXSA, -1, "XSA number to act upon")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// publishCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	cnaPublishCmd.Flags().BoolP(flagForce, "f", false, "Send even in the presence of errors")
}
